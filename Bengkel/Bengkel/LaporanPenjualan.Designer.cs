﻿namespace Bengkel
{
    partial class LaporanPenjualan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnjual = new System.Windows.Forms.Button();
            this.btnser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnjual
            // 
            this.btnjual.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnjual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnjual.Location = new System.Drawing.Point(41, 60);
            this.btnjual.Name = "btnjual";
            this.btnjual.Size = new System.Drawing.Size(203, 52);
            this.btnjual.TabIndex = 19;
            this.btnjual.Text = "PENJUALAN BARANG";
            this.btnjual.UseVisualStyleBackColor = false;
            this.btnjual.Click += new System.EventHandler(this.btnjual_Click);
            // 
            // btnser
            // 
            this.btnser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnser.Location = new System.Drawing.Point(41, 128);
            this.btnser.Name = "btnser";
            this.btnser.Size = new System.Drawing.Size(203, 52);
            this.btnser.TabIndex = 20;
            this.btnser.Text = "PENJUALAN SERVIS";
            this.btnser.UseVisualStyleBackColor = false;
            this.btnser.Click += new System.EventHandler(this.btnser_Click);
            // 
            // LaporanPenjualan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnser);
            this.Controls.Add(this.btnjual);
            this.Name = "LaporanPenjualan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LaporanPenjualan";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LaporanPenjualan_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnjual;
        private System.Windows.Forms.Button btnser;
    }
}