﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;

namespace Bengkel
{
    public partial class Formlaporanreturbeli : Form
    {
        Karyawan username;
        utama ut;
        Modelsuplier sup;
        public Formlaporanreturbeli(Karyawan username, utama ut)
        {
            this.username = username;
            this.ut = ut;
            InitializeComponent();
            defaultform_load();
        }
        public void defaultform_load()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql2 = "select Id_Suplier, Nama_Suplier,Alamat,Telpon from Suplier where Bengkel_id =@id and status = 0;";
            List<Modelsuplier> data2 = kon.Query<Modelsuplier>(sql2, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            foreach (var item in data2)
            {
                cbosuplier.Items.Add(item);
            }
        }


        private void btnok_Click(object sender, EventArgs e)
        {
            DateTime tgalmuali = dt1.Value;
            DateTime tglslsai = dt2.Value;
            string supl = label3.Text;
            if(tgalmuali>tglslsai)
            {
                MessageBox.Show("Tanggal Mulai Tidak Boleh melebihi tanggal selesai");
            }
            else if (cbosuplier.SelectedIndex == -1)
            {
                LAPORANRETURBELII LAP = new LAPORANRETURBELII(username, tgalmuali, tglslsai);
                LAP.Show();

            }
            else
            {
                laporanreturbeli lapo = new laporanreturbeli(username, tgalmuali, tglslsai, supl);
                lapo.Show();
            }
        }

        private void cbosuplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbosuplier.SelectedIndex > -1)
            {
                Modelsuplier suplier = (Modelsuplier)cbosuplier.SelectedItem;
                label3.Text = suplier.Nama_Suplier.ToString();

            }
        }
    }
}
