﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class UpKaryawan : Form
    {
        Karyawan username;
        string id;
        Karyawan kar;
        Karyawann k;
        BengkelEntities5 upp = new BengkelEntities5();
        public UpKaryawan(string id, Karyawan username,Karyawann k)
        {
            this.k = k;
            InitializeComponent();
            this.username = username;
            this.id = id;
            load();
        }
        public void load()
        {
            var data1 = (from x in upp.Karyawans where x.Id_Karyawan == id select x).FirstOrDefault<Karyawan>();
            txtnama.Text = data1.Nama;
            txtalamat.Text = data1.Alamat;
            txtsandi.Text = data1.Kata_sandi;
            txtkonfirmsandi.Text = data1.konfirmasi;
            txttelpon.Text = data1.Telpon;

            kar = data1;
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            kar.Nama = txtnama.Text;
            kar.Alamat = txtalamat.Text;
            kar.Kata_sandi = txtsandi.Text;
            kar.konfirmasi = txtkonfirmsandi.Text;
            kar.Telpon = txttelpon.Text;

            upp.SaveChanges();
            MessageBox.Show("Data Berhasil Diubah...");
        }

       

        private void txttelpon_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }

        private void UpKaryawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            k.load();
        }
    }
}
