﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class formlaporancs : Form
    {
        Karyawan username;
        public formlaporancs(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime tgalmuali = dt1.Value;

            DateTime tglslsai = dt2.Value;
            if(tgalmuali>tglslsai)
            {
                MessageBox.Show("Tanggal Mulai Tidak Boleh melebihi tanggal selesai");
            }
            else
            {
                laporanprint lapo = new laporanprint(username, tgalmuali, tglslsai);
                lapo.Show();
            }
            

        }

        private void dt1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dt2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
