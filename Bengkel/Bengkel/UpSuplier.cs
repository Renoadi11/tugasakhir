﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class UpSuplier : Form
    {
        Karyawan username;
        string id;
        Suplier sup;
        Suplierr s;
        BengkelEntities5 upp = new BengkelEntities5();
        public UpSuplier(string id, Karyawan username, Suplierr s)
        {
            this.s = s;
            this.username = username;
            InitializeComponent();
            this.id = id;
            load();
        }
        public void load()
        {
            var data1 = (from x in upp.Supliers where x.Id_Suplier == id select x).FirstOrDefault<Suplier>();
            txtnama.Text = data1.Nama_Suplier;
            txtalamat.Text = data1.Alamat;
            txttelpon.Text = data1.Telpon;

            sup = data1;
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            sup.Nama_Suplier = txtnama.Text;
            sup.Alamat = txtalamat.Text;
            sup.Telpon = txttelpon.Text;
            if (rdoaktif.Checked || rdotidak.Checked)
            {
                if (rdoaktif.Checked)
                {
                    sup.status = 0;

                }
               
            }
            else
            {
                sup = null;
                MessageBox.Show("Pilih status");
            }
            MessageBox.Show("Data Berhasil Diubah...");
            upp.SaveChanges();
        }

        private void txttelpon_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }

        private void UpSuplier_Load(object sender, EventArgs e)
        {

        }

        private void UpSuplier_FormClosing(object sender, FormClosingEventArgs e)
        {
            s.load();
        }
    }
}
