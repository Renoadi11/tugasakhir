﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class UpBarang : Form
    {
        Karyawan username;
        string id;
        Barangg b;
        Barang bar;
        BengkelEntities5 upp = new BengkelEntities5();
        public UpBarang(string id, Karyawan username,Barangg b)
        {
            this.b = b; 
            this.username = username;
            InitializeComponent();
            this.id = id;
            load();

        }
        public void load()
        {
            var data1 = (from x in upp.Barangs where x.Id_Barang == id select x).FirstOrDefault<Barang>();
            txtbarang.Text = data1.Nama_Barang;
            cbojenis.SelectedItem = data1.Jenis_Barang;
            // MessageBox.Show(cbojenis.Text);
            txtjumlahstok.Text = data1.Stok_Dimiliki.ToString();
            txthargajual.Text = data1.Harga_Satuan.ToString();
            txtrak.Text = data1.No_rak_barang;
            txtketerangan.Text = data1.Keterangan;
            txtpenambahanstok.Text = data1.penambahan_Stok.ToString();
            txthargapokok.Text = data1.Harga_Beli.ToString();
            txtstokminimall.Text = data1.Stok_minimal.ToString();
            bar = data1;
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            bar.Nama_Barang = txtbarang.Text;
            
            if(cbojenis.SelectedIndex >0)
            {
                bar.Jenis_Barang = cbojenis.SelectedItem.ToString();
            }
            bar.Stok_Dimiliki = Convert.ToInt32(txtjumlahstok.Text.ToString());
            bar.Harga_Satuan = Convert.ToDecimal(txthargajual.Text.ToString());
            bar.No_rak_barang = txtrak.Text;
            bar.Keterangan = txtketerangan.Text;
            bar.penambahan_Stok = Convert.ToInt32(txtpenambahanstok.Text);
            bar.Harga_Beli = Convert.ToDecimal(txthargapokok.Text.ToString());
            bar.penambahan_Stok = Convert.ToInt32(txtpenambahanstok.Text.ToString());

            MessageBox.Show("Data Berhasil Diubah...");

            upp.SaveChanges();
        }

        private void txtjumlahstok_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }

        private void txtstokminimall_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }

        private void txthargajual_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }

        private void txtpenambahanstok_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }

        private void txthargapokok_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }

        private void UpBarang_FormClosing(object sender, FormClosingEventArgs e)
        {
            b.load();
        }
    }
}
