﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;


namespace Bengkel
{
    public partial class TambahKeahliann : Form
    {
        Karyawan username;
        public TambahKeahliann(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
        }
        public string get_IdKeahlian()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetidKeahlian id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            kon.Close();
            return "KH" + id;
        }
        public void defaultform_load()
        {
            txtid.Text = get_IdKeahlian();

        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            Keahlian keh = new Keahlian();

            keh.Id_Keahlian = get_IdKeahlian();
            keh.Nama_Keahlian = txtnamakeahlian.Text;
            keh.Bengkel_id = username.Bengkel_id;

            context.Keahlians.Add(keh);
            context.SaveChanges();
            MessageBox.Show("data Berhasil di input");
            clear();

        }
        public void clear()
        {
            txtid.Text = "";
            txtnamakeahlian.Text = "";
        }
    }
}
