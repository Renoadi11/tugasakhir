﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
namespace Bengkel
{
    public partial class Login : Form
    {
        
        public Login()
        {
           
            InitializeComponent();
            this.Closing += new System.ComponentModel.CancelEventHandler(FormClosingEventCancle_Closing);
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnmasuk_Click(object sender, EventArgs e)
        {
            Karyawan User = userlogin(txtnama.Text, txtsan.Text);
            if (User != null)
            {
                utama tampil = new utama(User);
                tampil.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Pengguna dan kata sandi salah");
            }
        }
        private Karyawan userlogin(string username, string password)
        {
            Karyawan kar = null;

            try
            {
                BengkelEntities5 context = new BengkelEntities5();
                if (username != "" && password != "")
                {
                    kar = context.Karyawans.Where(s => s.Id_Karyawan == username && s.Kata_sandi == password).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }


            return kar;

        }
        private void FormClosingEventCancle_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Apakah yakin ingin keluar dari aplikasi ini?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;

            }


            else
            {
                e.Cancel = false;
                Application.Exit();
            }

        }
    }
}
