﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bengkel.Model;
using System.Globalization;
using System.IO;

namespace Bengkel
{
    public partial class Editpenjualanbarangservis : Form
    {
        Karyawan username;
        private ModelBarangNota barnot { get; set; }
        Nota_jual_barang not;
        BengkelEntities5 upp = new BengkelEntities5();
        Penjualan_serviss fpb;
        public Editpenjualanbarangservis(ModelBarangNota barnot, Penjualan_serviss fpb)
        {
            this.fpb = fpb;
            this.barnot = barnot;
            Nota_jual_barang not;
            InitializeComponent();
            defaultform_load();
            

        }


        public void defaultform_load()
        {
           
            txtbarangg.Text = barnot.NamaBarang;
            txtjumlah.Text = barnot.Jumlah.ToString();
            txtpotongan.Text = barnot.Potongan.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Editpenjualanbarangservis ed = this;

            foreach (ModelBarangNota item in fpb.datanota2.Where(u => u.IdBarang == barnot.IdBarang))
            {
                item.Jumlah = int.Parse(txtjumlah.Text);
                item.Potongan = int.Parse(txtpotongan.Text);

            }
            fpb.resetdatagridview();
            fpb.Focus();
            ed.Close();

        }
    }
}
