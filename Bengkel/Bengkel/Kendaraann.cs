﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Bengkel.Model;

namespace Bengkel
{
    public partial class Kendaraann : Form
    {
        Karyawan username;
        public Kendaraann(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
            load();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormtambahKendaraan tam = new FormtambahKendaraan(username);
            tam.Show();
        }
        private void load()
        {
           
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Kendaraan, Pemilik, Alamat, merk,tipe,Kode_Kendaraan,Kilometer  from Kendaraan where Bengkel_id =@id;";
            List<modelkendaraan> data = kon.Query<modelkendaraan>(sql, new {id=username.Bengkel_id}).ToList();
            kon.Close();

            dataGridView1.DataSource = data;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            UpKendaraan tambah = new UpKendaraan(up, username);
            tambah.Show();
            
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
