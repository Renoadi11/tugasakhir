﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;
using System.Configuration;

namespace Bengkel
{
    public partial class FormLaporanservis : Form
    {
        Karyawan username;
        modelmekanik mekaa;
        utama ua;

        public FormLaporanservis(Karyawan username,utama ua)
        {
            this.ua = ua;
            this.username = username;
            
            InitializeComponent();
            defaultform_load();
        }
        public void defaultform_load()
        {
           
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
             string sql = "select Id_Mekanik, nama, Alamat, telepon from Mekanik where bengkel_id = @id;";
            List<modelmekanik> data2 = kon.Query<modelmekanik>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            foreach (var item in data2)
            {
                cbomek.Items.Add(item);


            }
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            DateTime tgalmuali = dt1.Value;
            DateTime tglslsai = dt2.Value;
            string mekaa = label4.Text;
            if(tgalmuali>tglslsai)
            {
                MessageBox.Show("Tanggal Mulai Tidak Boleh melebihi tanggal selesai");
            }
            else if (cbomek.SelectedIndex == -1)
            {
                LAPORANSERVISUMUM lapor = new LAPORANSERVISUMUM(username, tgalmuali, tglslsai);
                lapor.Show();
            }
            else
            {
                LaporanServis lapo = new LaporanServis(username, tgalmuali, tglslsai,mekaa);
                lapo.Show();
            }

        }

        private void cbomek_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbomek.SelectedIndex > -1)
            {
                modelmekanik meka = (modelmekanik)cbomek.SelectedItem;
                label4.Text =meka.Nama.ToString();

            }
        }
    }
}
