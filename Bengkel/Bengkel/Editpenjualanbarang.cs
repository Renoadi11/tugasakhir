﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;
using System.IO;



namespace Bengkel
{
    public partial class Editpenjualanbarang : Form
    {
        Karyawan username;
        private ModelBarangNota barnot { get; set; }
        Nota_jual_barang not;
        BengkelEntities5 upp = new BengkelEntities5();
        FormPENJUALAN_BARANGG fpb;
        public Editpenjualanbarang(ModelBarangNota barnot, FormPENJUALAN_BARANGG fpb)
        {
            this.fpb = fpb;
            this.barnot = barnot;
            Nota_jual_barang not;
            InitializeComponent();
            defaultform_load();
        }
        public void defaultform_load()
        {
            txtbarangg.Text = barnot.NamaBarang;
            txtjumlah.Text = barnot.Jumlah.ToString();
            txtpotongan.Text = barnot.Potongan.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Editpenjualanbarang ed = this;
           
            foreach (ModelBarangNota item in fpb.datanota.Where(u => u.IdBarang == barnot.IdBarang))
            {
                item.Jumlah = int.Parse(txtjumlah.Text);
                item.Potongan = int.Parse(txtpotongan.Text);
              
            }
            fpb.resetdatagridview();
            fpb.Focus();
            ed.Close();
            
            

        }
    }
}
