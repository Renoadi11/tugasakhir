﻿namespace Bengkel
{
    partial class Bookingg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblmekanik = new System.Windows.Forms.Label();
            this.dtpiker = new System.Windows.Forms.DateTimePicker();
            this.lbladmin = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cbomekanik = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtketerangan = new System.Windows.Forms.TextBox();
            this.lblbooking = new System.Windows.Forms.Label();
            this.txtalmt = new System.Windows.Forms.TextBox();
            this.txttlp = new System.Windows.Forms.TextBox();
            this.txtnama = new System.Windows.Forms.TextBox();
            this.Transaksi = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtpotongan = new System.Windows.Forms.TextBox();
            this.btntmbh = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label11 = new System.Windows.Forms.Label();
            this.txtjml = new System.Windows.Forms.TextBox();
            this.cbobarser = new System.Windows.Forms.ComboBox();
            this.txtsub = new System.Windows.Forms.TextBox();
            this.btnsimpan = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.Transaksi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tanggal Booking :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(64, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nama :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(339, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Alamat :";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(50, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Telepon :";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(312, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Keterangan :";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.lblmekanik);
            this.groupBox1.Controls.Add(this.dtpiker);
            this.groupBox1.Controls.Add(this.lbladmin);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.cbomekanik);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtketerangan);
            this.groupBox1.Controls.Add(this.lblbooking);
            this.groupBox1.Controls.Add(this.txtalmt);
            this.groupBox1.Controls.Add(this.txttlp);
            this.groupBox1.Controls.Add(this.txtnama);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(13, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(667, 210);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(35, 15);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Id.Booking :";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(0, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 17);
            this.label10.TabIndex = 11;
            this.label10.Text = "BOOKING";
            // 
            // lblmekanik
            // 
            this.lblmekanik.AutoSize = true;
            this.lblmekanik.Location = new System.Drawing.Point(551, 21);
            this.lblmekanik.Name = "lblmekanik";
            this.lblmekanik.Size = new System.Drawing.Size(41, 13);
            this.lblmekanik.TabIndex = 17;
            this.lblmekanik.Text = "label7";
            // 
            // dtpiker
            // 
            this.dtpiker.CustomFormat = "dd/MM/yyyy hh:mm:ss tt";
            this.dtpiker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpiker.Location = new System.Drawing.Point(135, 47);
            this.dtpiker.Name = "dtpiker";
            this.dtpiker.Size = new System.Drawing.Size(181, 20);
            this.dtpiker.TabIndex = 16;
            // 
            // lbladmin
            // 
            this.lbladmin.Location = new System.Drawing.Point(0, 0);
            this.lbladmin.Name = "lbladmin";
            this.lbladmin.Size = new System.Drawing.Size(100, 23);
            this.lbladmin.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 23);
            this.label13.TabIndex = 19;
            // 
            // cbomekanik
            // 
            this.cbomekanik.FormattingEnabled = true;
            this.cbomekanik.Location = new System.Drawing.Point(405, 13);
            this.cbomekanik.Name = "cbomekanik";
            this.cbomekanik.Size = new System.Drawing.Size(140, 21);
            this.cbomekanik.TabIndex = 13;
            this.cbomekanik.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(329, 21);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 12;
            this.label12.Text = "Mekanik :";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // txtketerangan
            // 
            this.txtketerangan.Location = new System.Drawing.Point(405, 114);
            this.txtketerangan.Multiline = true;
            this.txtketerangan.Name = "txtketerangan";
            this.txtketerangan.Size = new System.Drawing.Size(247, 93);
            this.txtketerangan.TabIndex = 11;
            this.txtketerangan.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // lblbooking
            // 
            this.lblbooking.AutoSize = true;
            this.lblbooking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbooking.Location = new System.Drawing.Point(132, 15);
            this.lblbooking.Name = "lblbooking";
            this.lblbooking.Size = new System.Drawing.Size(41, 13);
            this.lblbooking.TabIndex = 9;
            this.lblbooking.Text = "label7";
            this.lblbooking.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtalmt
            // 
            this.txtalmt.Location = new System.Drawing.Point(405, 38);
            this.txtalmt.Multiline = true;
            this.txtalmt.Name = "txtalmt";
            this.txtalmt.Size = new System.Drawing.Size(247, 62);
            this.txtalmt.TabIndex = 8;
            this.txtalmt.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // txttlp
            // 
            this.txttlp.Location = new System.Drawing.Point(117, 114);
            this.txttlp.Name = "txttlp";
            this.txttlp.Size = new System.Drawing.Size(116, 20);
            this.txttlp.TabIndex = 7;
            this.txttlp.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.txttlp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txttlp_KeyPress);
            // 
            // txtnama
            // 
            this.txtnama.Location = new System.Drawing.Point(117, 80);
            this.txtnama.Name = "txtnama";
            this.txtnama.Size = new System.Drawing.Size(116, 20);
            this.txtnama.TabIndex = 6;
            this.txtnama.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Transaksi
            // 
            this.Transaksi.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Transaksi.Controls.Add(this.label21);
            this.Transaksi.Controls.Add(this.label20);
            this.Transaksi.Controls.Add(this.label8);
            this.Transaksi.Controls.Add(this.label7);
            this.Transaksi.Controls.Add(this.txtpotongan);
            this.Transaksi.Controls.Add(this.btntmbh);
            this.Transaksi.Controls.Add(this.label18);
            this.Transaksi.Controls.Add(this.dataGridView1);
            this.Transaksi.Controls.Add(this.label11);
            this.Transaksi.Controls.Add(this.txtjml);
            this.Transaksi.Controls.Add(this.cbobarser);
            this.Transaksi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Transaksi.Location = new System.Drawing.Point(13, 238);
            this.Transaksi.Name = "Transaksi";
            this.Transaksi.Size = new System.Drawing.Size(667, 140);
            this.Transaksi.TabIndex = 7;
            this.Transaksi.TabStop = false;
            this.Transaksi.Text = "Transaksi";
            this.Transaksi.Enter += new System.EventHandler(this.Transaksi_Enter);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(468, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Potongan :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(196, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Masukkan Nama Barang / servis:";
            // 
            // txtpotongan
            // 
            this.txtpotongan.Location = new System.Drawing.Point(537, 16);
            this.txtpotongan.Name = "txtpotongan";
            this.txtpotongan.Size = new System.Drawing.Size(76, 20);
            this.txtpotongan.TabIndex = 9;
            this.txtpotongan.Text = "0";
            // 
            // btntmbh
            // 
            this.btntmbh.Location = new System.Drawing.Point(632, 17);
            this.btntmbh.Name = "btntmbh";
            this.btntmbh.Size = new System.Drawing.Size(29, 23);
            this.btntmbh.TabIndex = 8;
            this.btntmbh.Text = ">";
            this.btntmbh.UseVisualStyleBackColor = true;
            this.btntmbh.Click += new System.EventHandler(this.btntmbh_Click);
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(0, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(129, 23);
            this.label18.TabIndex = 10;
            this.label18.Text = "BARANG/SERVIS";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(0, 49);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(652, 85);
            this.dataGridView1.TabIndex = 5;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(368, 20);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Jumlah :";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // txtjml
            // 
            this.txtjml.Location = new System.Drawing.Point(425, 17);
            this.txtjml.Name = "txtjml";
            this.txtjml.Size = new System.Drawing.Size(38, 20);
            this.txtjml.TabIndex = 3;
            this.txtjml.Text = "1";
            this.txtjml.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // cbobarser
            // 
            this.cbobarser.FormattingEnabled = true;
            this.cbobarser.Location = new System.Drawing.Point(220, 16);
            this.cbobarser.Name = "cbobarser";
            this.cbobarser.Size = new System.Drawing.Size(140, 21);
            this.cbobarser.TabIndex = 2;
            this.cbobarser.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // txtsub
            // 
            this.txtsub.Location = new System.Drawing.Point(565, 378);
            this.txtsub.Multiline = true;
            this.txtsub.Name = "txtsub";
            this.txtsub.Size = new System.Drawing.Size(115, 31);
            this.txtsub.TabIndex = 8;
            // 
            // btnsimpan
            // 
            this.btnsimpan.Location = new System.Drawing.Point(13, 382);
            this.btnsimpan.Name = "btnsimpan";
            this.btnsimpan.Size = new System.Drawing.Size(82, 34);
            this.btnsimpan.TabIndex = 10;
            this.btnsimpan.Text = "Simpan";
            this.btnsimpan.UseVisualStyleBackColor = true;
            this.btnsimpan.Click += new System.EventHandler(this.btnsimpan_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(318, 21);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(50, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(12, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "*";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(301, 117);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(12, 13);
            this.label15.TabIndex = 28;
            this.label15.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(38, 114);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(12, 13);
            this.label16.TabIndex = 29;
            this.label16.Text = "*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(330, 53);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(12, 13);
            this.label17.TabIndex = 30;
            this.label17.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(-3, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(12, 13);
            this.label19.TabIndex = 31;
            this.label19.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(6, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(12, 13);
            this.label20.TabIndex = 32;
            this.label20.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(361, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(12, 13);
            this.label21.TabIndex = 33;
            this.label21.Text = "*";
            // 
            // Bookingg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(694, 428);
            this.Controls.Add(this.btnsimpan);
            this.Controls.Add(this.txtsub);
            this.Controls.Add(this.Transaksi);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Bookingg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bookingg";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Bookingg_FormClosing);
            this.Load += new System.EventHandler(this.Bookingg_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Transaksi.ResumeLayout(false);
            this.Transaksi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtalmt;
        private System.Windows.Forms.TextBox txttlp;
        private System.Windows.Forms.TextBox txtnama;
        private System.Windows.Forms.TextBox txtketerangan;
        private System.Windows.Forms.Label lblbooking;
        private System.Windows.Forms.GroupBox Transaksi;
        private System.Windows.Forms.TextBox txtjml;
        private System.Windows.Forms.ComboBox cbobarser;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lbladmin;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cbomekanik;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtsub;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnsimpan;
        private System.Windows.Forms.Button btntmbh;
        private System.Windows.Forms.TextBox txtpotongan;
        private System.Windows.Forms.DateTimePicker dtpiker;
        private System.Windows.Forms.Label lblmekanik;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
    }
}