﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class Lpembelian : Form
    {
        Karyawan username;
        utama ut;
        public Lpembelian(Karyawan username, utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
        }

        private void btnok_Click(object sender, EventArgs e)
        {
            Formlaporanpembelian lapo = new Formlaporanpembelian(username,ut);
            lapo.Hide();
            lapo.Show();
        }

        private void Lpembelian_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }

        private void Lpembelian_Load(object sender, EventArgs e)
        {

        }
    }
}
