﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Bengkel.Model;


namespace Bengkel
{
    public partial class Barangg : Form
    {
        Karyawan username;
        utama ut;
        public Barangg(Karyawan username, utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            load();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Tambah_Barang tam = new Tambah_Barang(username,this);
            tam.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            UpBarang tambah = new UpBarang(up, username,this);
            tambah.Show();
        }
        public void load()
        {
            dataGridView1.DataSource = null;
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Barang, Nama_Barang,Jenis_Barang,Harga_Satuan, convert(decimal,Harga_Beli) as Harga_Beli ,No_rak_barang,Stok_Dimiliki,Stok_minimal,Status_barang,penambahan_Stok from Barang where Bengkel_id=@id and Hapus=0 order by Tipe_barang asc; ";
            List<ModelBarang> data = kon.Query<ModelBarang>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            dataGridView1.DataSource = data;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Barangg_Load(object sender, EventArgs e)
        {
            ut.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            var hapus = (from c in context.Barangs where c.Id_Barang == up select c).FirstOrDefault();
            hapus.Hapus = 1;
            context.SaveChanges();
            MessageBox.Show("Berhasil Dihapus");
            load();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
