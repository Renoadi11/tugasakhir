//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bengkel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Karyawan
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Karyawan()
        {
            this.Barangs = new HashSet<Barang>();
            this.Nota_jual_barang = new HashSet<Nota_jual_barang>();
            this.Nota_Pembelian_Barang = new HashSet<Nota_Pembelian_Barang>();
            this.Nota_servis = new HashSet<Nota_servis>();
        }
    
        public string Id_Karyawan { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Telpon { get; set; }
        public string Kata_sandi { get; set; }
        public string konfirmasi { get; set; }
        public string Bengkel_id { get; set; }
        public Nullable<int> Status_pengguna { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Barang> Barangs { get; set; }
        public virtual Master_Bengkel Master_Bengkel { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_jual_barang> Nota_jual_barang { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_Pembelian_Barang> Nota_Pembelian_Barang { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_servis> Nota_servis { get; set; }
    }
}
