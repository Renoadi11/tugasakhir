//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bengkel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Master_Bengkel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Master_Bengkel()
        {
            this.Barangs = new HashSet<Barang>();
            this.Jasa_Servis = new HashSet<Jasa_Servis>();
            this.Karyawans = new HashSet<Karyawan>();
            this.Keahlians = new HashSet<Keahlian>();
            this.Kendaraans = new HashSet<Kendaraan>();
            this.Nota_jual_barang = new HashSet<Nota_jual_barang>();
            this.Nota_Pembelian_Barang = new HashSet<Nota_Pembelian_Barang>();
            this.Nota_retur_Jual_barang = new HashSet<Nota_retur_Jual_barang>();
            this.Nota_Retur_Pembelian_barang = new HashSet<Nota_Retur_Pembelian_barang>();
            this.Nota_Retur_Pembelian_barang1 = new HashSet<Nota_Retur_Pembelian_barang>();
            this.Nota_servis = new HashSet<Nota_servis>();
            this.Supliers = new HashSet<Suplier>();
        }
    
        public string Id_Bengkel { get; set; }
        public string Alamat { get; set; }
        public string Telepon { get; set; }
        public string logo { get; set; }
        public string Nama { get; set; }
        public Nullable<int> Diskon { get; set; }
        public Nullable<int> Retur { get; set; }
        public Nullable<int> Booking { get; set; }
        public Nullable<int> retur_jual { get; set; }
        public Nullable<int> fee { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Barang> Barangs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Jasa_Servis> Jasa_Servis { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Karyawan> Karyawans { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Keahlian> Keahlians { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Kendaraan> Kendaraans { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_jual_barang> Nota_jual_barang { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_Pembelian_Barang> Nota_Pembelian_Barang { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_retur_Jual_barang> Nota_retur_Jual_barang { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_Retur_Pembelian_barang> Nota_Retur_Pembelian_barang { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_Retur_Pembelian_barang> Nota_Retur_Pembelian_barang1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Nota_servis> Nota_servis { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Suplier> Supliers { get; set; }
    }
}
