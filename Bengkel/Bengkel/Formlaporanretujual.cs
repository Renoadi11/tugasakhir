﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class Formlaporanretujual : Form
    {
        Karyawan username;
        public Formlaporanretujual(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime tgalmuali = dt1.Value;
            DateTime tglslsai = dt2.Value;
            if(tgalmuali>tglslsai)
            {
                MessageBox.Show("Tanggal Mulai Tidak Boleh melebihi tanggal selesai");
            }
            else
            {
                laporanreturjual lapo = new laporanreturjual(username, tgalmuali, tglslsai);
                lapo.Show();
            }
           
        }
    }
}
