﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class TambahJasa : Form
    {
        Karyawan username;
        public TambahJasa(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
        }
        public string get_IdJasa()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for Getidjasa id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            kon.Close();
            return "JS" + id;
        }
        public void defaultform_load()
        {
            lblidd.Text = get_IdJasa();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            Jasa_Servis jas = new Jasa_Servis();

            jas.Id_Jasa = get_IdJasa();
            jas.Nama_jasa = txtnama.Text;
            jas.Harga = Convert.ToDecimal(txtharga.Text.ToString());
            jas.Bengek_id = username.Bengkel_id;

            context.Jasa_Servis.Add(jas);
            context.SaveChanges();
            MessageBox.Show("data Berhasil di input");
            clear();


        }
        public void clear()
        {
            txtnama.Text = "";
            txtharga.Text = "";

        }

        private void txtharga_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }
    }
    
}
