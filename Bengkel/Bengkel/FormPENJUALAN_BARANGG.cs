﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;


namespace Bengkel
{
    public partial class FormPENJUALAN_BARANGG : Form
    {
        Karyawan username;
        utama ut;
        public  List<ModelBarangNota> datanota = new List<ModelBarangNota>();
        public FormPENJUALAN_BARANGG(Karyawan username,utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            defaultform_load();
            lbladmin.Text = username.Nama;
        }
        public string get_idNotajualbarang()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select CONCAT(YEAR( GETDATE()),RIGHT('00' + CONVERT(VARCHAR(12),  MONTH(GETDATE())), 2),RIGHT('0000' + CONVERT(VARCHAR(12), next value for GetIdNotajualbarang ), 4)) id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "SO/" + id;
        }
        public void defaultform_load()
        {
            
            lblnotransaksi.Text = get_idNotajualbarang();
            lbltgl.Text = DateTime.Now.ToString();
            

            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Barang, Nama_Barang, Jenis_Barang, Harga_Satuan, Harga_Beli, No_rak_barang, Stok_Dimiliki, Stok_minimal, Status_barang, penambahan_Stok from Barang where Bengkel_id = @id and Tipe_Barang = @tp;";
            List<ModelBarang> data = kon.Query<ModelBarang>(sql, new { id = username.Bengkel_id, tp = "1" }).ToList();
            kon.Close();

           // dataGridView1.DataSource = data;
            foreach (var item in data)
            {
                cbobarang.Items.Add(item);
            }
            
        }
        public void resetdatagridview()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = datanota;

            txtttotal.Text = datanota.Sum(i => i.SubTotal).ToString();
            txttotal.Text = datanota.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));
            txtsisa.Text = datanota.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));


        }





        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cbobarang_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            ModelBarang barang =(ModelBarang)cbobarang.SelectedItem;
            var jumlah = txtjumlah.Text;
            var potongan = txtpotongan.Text;
            var harga = barang.Harga_Satuan;
            
            ModelBarangNota item = new ModelBarangNota();

            item.IdBarang = barang.Id_Barang;
            item.Jumlah =int.Parse(jumlah);
            item.NamaBarang = barang.Nama_Barang;
            item.Potongan = int.Parse(potongan);
            item.Harga = barang.Harga_Satuan;

            



            datanota.Add(item);
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = datanota;

            txtttotal.Text = datanota.Sum(i => i.SubTotal).ToString();
            txttotal.Text = datanota.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));
            txtsisa.Text = datanota.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));
            
            



        }

        private void button5_Click(object sender, EventArgs e)
        {

            ModelBarangNota barnot =(ModelBarangNota) dataGridView1.CurrentRow.DataBoundItem;
            Editpenjualanbarang ed = new Editpenjualanbarang(barnot,this);
            ed.Show();

        }
        public string get_iddetailnota()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIddetailnotajual id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "DT" + id;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                BengkelEntities5 context = new BengkelEntities5();
                Nota_jual_barang not = new Nota_jual_barang();

                not.Id_nota_Jual = get_idNotajualbarang();
                not.Karyawan_Id = username.Id_Karyawan;
                not.Bengkel_id = username.Bengkel_id;
                not.Tanggal_Jual = DateTime.Now;
                


                context.Nota_jual_barang.Add(not);



                foreach (var item in datanota)
                {
                    
                    BengkelEntities5 conte = new BengkelEntities5();
                    Detail_Jual_Barang det = new Detail_Jual_Barang();
                    det.Id_detail_nota_jual = get_iddetailnota();
                    det.id_nota_jual = not.Id_nota_Jual;
                    det.id_barang = item.IdBarang;
                    det.Nama_Barang = item.NamaBarang;
                    det.Harga = item.Harga;
                    det.potongan = item.Potongan;
                    det.Jumlah = item.Jumlah;
                    det.Sub_total = item.SubTotal;

                    context.Detail_Jual_Barang.Add(det);


                }

                context.SaveChanges();



                MessageBox.Show("Data Berhasil di Simpan");

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           

        }
        
        public string get_IdDetail()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIddetailnotajual id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            kon.Close();
            return "DT" + id;

        }

        private void textBox9_KeyUp(object sender, KeyEventArgs e)
        {
            if (txttotal.Text != "" && txtbayar.Text != "")
            {
                int total = int.Parse(txtttotal.Text);
                int bayar = int.Parse(txtbayar.Text);
                if (bayar > 0)
                {
                    txtsisabayar.Text = (bayar - total).ToString();
                    txtsisa.Text = (bayar - total).ToString("C", new CultureInfo("id-ID"));
                    // if(total =! null && bayar)
                }


            }
            else
            {
                txtsisabayar.Text = null;
                txtsisa.Text = null;
            }


        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void FormPENJUALAN_BARANGG_FormClosed(object sender, FormClosedEventArgs e)
        {
            ut.Show();
        }

        private void btnhps_Click(object sender, EventArgs e)
        {
            ModelBarangNota barnot = (ModelBarangNota)dataGridView1.CurrentRow.DataBoundItem;
            datanota.RemoveAll(r => r.IdBarang == barnot.IdBarang);
            resetdatagridview();
        }

        private void FormPENJUALAN_BARANGG_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
