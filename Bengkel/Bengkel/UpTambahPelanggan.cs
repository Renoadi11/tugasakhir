﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class UpTambahPelanggan : Form
    {
        string id;
        Pelanggan pel;
        BengkelEntities5 upp = new BengkelEntities5();
        public UpTambahPelanggan(string id)
        {
            InitializeComponent();
            this.id = id;
            load();
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            pel.Nama_pelanggan = txtnama.Text;
            pel.Alamat_Pelanggan = txtalamat.Text;
            pel.Telpon = txttelepon.Text;
            if (rdolaki.Checked)
            {
                pel.Jenis_Kelamin = rdolaki.Text;
               
            }
            else
            {
                pel.Jenis_Kelamin = rdoperempuan.Text;
               
            }
            pel.Tanggal_Terakhir_Servis = dateTimePicker1.Value;
            if (rdoya.Checked)
            {
                string Sudah_diingatkan = rdoya.Text;

            }
            else
            {
                string Sudah_diingatkan = rdobelum.Text;

            }

            MessageBox.Show("Data Berhasil Diubah...");
            upp.SaveChanges();
            
      
        }
        public void load()
        {
           
            var data1 = (from x in upp.Pelanggans where x.Id_pelanggan == id select x).FirstOrDefault<Pelanggan>();
            txtnama.Text = data1.Nama_pelanggan;
            txtalamat.Text = data1.Alamat_Pelanggan;
            txttelepon.Text = data1.Telpon;
            if (data1.Jenis_Kelamin=="LAKI")
            {
               rdolaki.Checked= true;
             
            }
            else
            {
                rdoperempuan.Checked = true;
                
            }
              dateTimePicker1.Value = data1.Tanggal_Terakhir_Servis;
            if (data1.Sudah_diingatkan =="YA")
            {
                rdoya.Checked = true;

            }
            else
            {
                rdobelum.Checked = true;
            }
            pel = data1;

        }

        private void txttelepon_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }
    }
}
