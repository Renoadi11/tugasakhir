﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class UcPembelian : UserControl
    {
        utama ut;
        Karyawan ky;
        public UcPembelian(utama ut, Karyawan ky)
        {
            this.ut = ut;
            this.ky = ky;
            InitializeComponent();
            load();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            PembelianBarangg tampil = new PembelianBarangg(ky, ut);
            ut.Hide();
            tampil.Show();
        }
        public void load()
        {

            BengkelEntities5 context = new BengkelEntities5();
            var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            if (ben.Retur == 0)
            {
                button8.Hide();
            }
            

        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Retur_Belii ret = new Retur_Belii(ky, ut);
            ut.Hide();
            ret.Show();
        }
    }
}
