﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Bengkel.Model;


namespace Bengkel
{
    public partial class Jasaa : Form
    {
        Karyawan username;
        public Jasaa(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
            load();
        }
        private void load()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Jasa,Nama_jasa,Harga from Jasa_Servis where Bengek_id=@id;";
            List<ModelJasa> data = kon.Query<ModelJasa>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();

            dataGridView1.DataSource = data;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TambahJasa jas = new TambahJasa(username);
            jas.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            UpJasa jas = new UpJasa(up, username);
            jas.Show();
        }
    }
}
