﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using Bengkel.Model;
using Dapper;

namespace Bengkel
{
    public partial class Mekanikk : Form
    {
        Karyawan username;
        utama ut;
        
        public Mekanikk(Karyawan username, utama ut)
        {
           
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            load();
        }

        private void Mekanikk_Load(object sender, EventArgs e)
        {
            ut.Show();
        }
        public void load()
        {
            dataGridView1.DataSource = null;
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Mekanik,nama,Alamat,telepon from Mekanik where bengkel_id=@id;";
            List<modelmekanik> data = kon.Query<modelmekanik>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            dataGridView1.DataSource = data;



        }

        private void button3_Click(object sender, EventArgs e)
        {
            TambahMekanikk tam = new TambahMekanikk(username,this);
            tam.Show();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            upmekanik tambah = new upmekanik(up, username,this);
            tambah.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            var hapus = (from c in context.Mekaniks where c.Id_Mekanik == up select c).FirstOrDefault();
            context.Mekaniks.Remove(hapus);
            context.SaveChanges();
            MessageBox.Show("Berhasil Dihapus");
            load();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
