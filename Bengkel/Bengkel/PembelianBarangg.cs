﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;
namespace Bengkel
{
    public partial class PembelianBarangg : Form
    {
        Karyawan username;
        Modelsuplier supl; 
        utama ut;
        
        public List<modelbarangnotajual> datanotaa = new List<modelbarangnotajual>();
        public PembelianBarangg(Karyawan username, utama ut)
        {
            
            //this.supl = supl;
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            defaultform_load();
            lbladmin.Text = username.Nama;
            load();
        }
        public void load()
        {

            BengkelEntities5 context = new BengkelEntities5();
            var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == username.Bengkel_id select bk).FirstOrDefault();
            if (ben.Diskon == 0)
            {
                txtpotongan.Hide();
                textBox1.Hide();
                label3.Hide();
                label8.Hide();
            }
            


        }
        public string get_idnotabeli()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select CONCAT(YEAR( GETDATE()),RIGHT('00' + CONVERT(VARCHAR(12),  MONTH(GETDATE())), 2),RIGHT('0000' + CONVERT(VARCHAR(12), next value for GetIdnotapembelian), 4)) id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "PO/" + id;
        }
        public void defaultform_load()
        {
            cbobarang.Items.Clear();
            lblnotransaksi.Text = get_idnotabeli();
            lbltgl.Text = DateTime.Now.ToString();


            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Barang, Nama_Barang,Jenis_Barang,Harga_Satuan,Harga_Beli,No_rak_barang,Stok_Dimiliki,Stok_minimal,Status_barang,penambahan_Stok from Barang where Bengkel_id =@id and Tipe_Barang =@tp and Hapus=0;";
           
            string sql2 = "select Id_Suplier, Nama_Suplier,Alamat,Telpon from Suplier where Bengkel_id =@id and status = 0;";
          //  List<ModelBarrang> data = kon.Query<ModelBarrang>(sql, new { id = username.Bengkel_id, tp = "2" }).ToList();
            List<ModelBarrang> data3 = kon.Query<ModelBarrang>(sql, new { id = username.Bengkel_id, tp = "1" }).ToList();
            List<Modelsuplier> data2 = kon.Query<Modelsuplier>(sql2, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            foreach (var item in data3)
            {
                cbobarang.Items.Add(item);

            }
            foreach (var item in data2)
            {
                cbosupplier.Items.Add(item);
            }

        }
        public void resetdatagridview()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = datanotaa;

            txtttotal.Text = datanotaa.Sum(i => i.SubTotal).ToString();
            label.Text = datanotaa.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));
            txtsisa.Text = datanotaa.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Pembelian_Barangg_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ModelBarrang barang = (ModelBarrang)cbobarang.SelectedItem;
            var jumlah = txtjumlah.Text;
            var potongan = txtpotongan.Text;
            var harga = txthharga.Text;
            if (cbosupplier.SelectedIndex == -1)
            {
                MessageBox.Show("PILIH NAMA SUPPLIER TERLEBIH DAHULU");
            }
            modelbarangnotajual item = new modelbarangnotajual();
            
            if (cbobarang.SelectedIndex == -1)
            {
                MessageBox.Show("PILIH BARANG TERLEBIH DAHULU");
            }
            if (txthharga.Text == "")
            {
                MessageBox.Show("ISI HARGA YERLEBI DAHULU");
            }
            else
            {
                item.IdBarang = barang.Id_Barang;
                item.Jumlah = int.Parse(jumlah);
                item.NamaBarang = barang.Nama_Barang;
                item.Potongan = int.Parse(potongan);
                item.Harga = int.Parse(harga);
                harga = txthharga.Text;
                

                datanotaa.Add(item);
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = datanotaa;
                txtttotal.Text = datanotaa.Sum(i => i.SubTotal).ToString();
                txtttotal.Text = datanotaa.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));
                txtsisa.Text = datanotaa.Sum(i => i.SubTotal).ToString("C", new CultureInfo("id-ID"));
            }
            

           

        }

        private void button5_Click(object sender, EventArgs e)
        {
            modelbarangnotajual barnotj = (modelbarangnotajual)dataGridView1.CurrentRow.DataBoundItem;
            Edit_Pembeliann ed = new Edit_Pembeliann(barnotj, this);
            ed.Show();
        }

        private void btnhps_Click(object sender, EventArgs e)
        {

            modelbarangnotajual da = (modelbarangnotajual)dataGridView1.CurrentRow.DataBoundItem;
            datanotaa.RemoveAll(r => r.IdBarang == da.IdBarang);
            resetdatagridview();

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        //private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (cbosupplier.SelectedIndex > -1)
        //    {
        //        Modelsuplier suplier = (Modelsuplier)cbosupplier.SelectedItem;
        //        lblalamat.Text = suplier.Alamat.ToString();
        //        lbltelo.Text = suplier.Telpon.ToString();
        //    }
        //}

        private void cbobarang_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public string get_iddetailnotabelii()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIdDetailNotaBeli id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "DTB" + id;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void cbosupplier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbosupplier.SelectedIndex > -1)
            {
                Modelsuplier suplier = (Modelsuplier)cbosupplier.SelectedItem;
                lblalamat.Text = suplier.Alamat.ToString();
                lbltelo.Text = suplier.Telpon.ToString();
                lblalamat.Visible = true;
                lbltelo.Visible = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            try
            {

                BengkelEntities5 context = new BengkelEntities5();
                Nota_Pembelian_Barang notbeli = new Nota_Pembelian_Barang();
                notbeli.jumlah_bayar = int.Parse(txtbayar.Text);
                supl = (Modelsuplier)cbosupplier.SelectedItem;
                notbeli.Id_nota_pembelian = lblnotransaksi.Text;
                notbeli.Suplier_Id = supl.Id_Suplier;
                notbeli.total = datanotaa.Sum(i => i.SubTotal);
                notbeli.Bengkel_id = username.Bengkel_id;
                notbeli.tanggal_Beli = DateTime.Now;
                int total1 = datanotaa.Sum(i => i.SubTotal);

                if (txtsisa.Text != "")
                {
                    int bayar = int.Parse(txtbayar.Text);
                    notbeli.kembalian = (bayar - total1);
                }


                if (txtpotongan.Text != "")
                {
                    notbeli.potongan = int.Parse(textBox1.Text);
                }



               
                context.Nota_Pembelian_Barang.Add(notbeli);





                //context.SaveChanges();
                foreach (var item in datanotaa)
                {
                    BengkelEntities5 conte = new BengkelEntities5();
                    Detail_nota_pembelian detnobl = new Detail_nota_pembelian();

                    //detnobl.id_detail_nota_pembelian = get_iddetailnotabelii();
                    detnobl.id_nota_pembelian = notbeli.Id_nota_pembelian;

                    detnobl.barang_id = item.IdBarang;
                    detnobl.Nama = item.NamaBarang;
                    detnobl.Harga = item.Harga;
                    detnobl.jumlah = item.Jumlah;
                    detnobl.potonagn = item.Potongan;
                    detnobl.sub_total = item.SubTotal;
                    context.Detail_nota_pembelian.Add(detnobl);
                    context.SaveChanges();
                    MessageBox.Show("Data Berhasil di Simpan");
                }
               
            }

            catch (Exception ex)
            {

                MessageBox.Show("Isi sesuai Dengan Transaksi Pembelian");
            }
           
        }

        private void PembelianBarangg_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }

        private void lblalamat_Click(object sender, EventArgs e)
        {

        }
    }
}
