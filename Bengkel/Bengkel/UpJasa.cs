﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;


namespace Bengkel
{
    public partial class UpJasa : Form
    {
         Karyawan username;
        string id;
        Jasa_Servis jas;
        BengkelEntities5 upp = new BengkelEntities5();
        public UpJasa(string id, Karyawan username)
        {
            this.username = username;
            this.id = id;
            InitializeComponent();
            load();
        }
        public void load()
        {
            var data1 = (from x in upp.Jasa_Servis where x.Id_Jasa == id select x).FirstOrDefault<Jasa_Servis>();
            txtnama.Text = data1.Nama_jasa;
            txtharga.Text = data1.Harga.ToString();
            jas= data1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            jas.Nama_jasa = txtnama.Text;
            jas.Harga = Convert.ToDecimal(txtharga.Text.ToString());

            upp.SaveChanges();
            MessageBox.Show("Data Berhasil Diubah...");
        }

        private void txtharga_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }
    }
    
}
