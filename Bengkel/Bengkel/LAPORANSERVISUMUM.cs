﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Bengkel
{
    public partial class LAPORANSERVISUMUM : Form
    {
        Karyawan username;
        DateTime mulai, selesai;

       

        public LAPORANSERVISUMUM(Karyawan username, DateTime mulai, DateTime selesai)
        {
            this.username = username;
            this.mulai = mulai;
            this.selesai = selesai;
            InitializeComponent();
        }
        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            BengkelEntities5 contex = new BengkelEntities5();
            Master_Bengkel data = contex.Master_Bengkel.Where(c => c.Id_Bengkel == username.Bengkel_id).FirstOrDefault();
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            string filename = path + "\\FOTO\\" + data.logo;

            CrystalReport7 laporanbarang = new CrystalReport7();
            laporanbarang.SetParameterValue("@bengkelid", username.Bengkel_id.ToString());
            laporanbarang.SetParameterValue("@tglmulai", mulai);
            laporanbarang.SetParameterValue("@tglslsai", selesai);
            laporanbarang.SetParameterValue("@tipe", "2");
            
            laporanbarang.SetParameterValue("rlupload", filename);
            laporanbarang.SetParameterValue("parambengkel", data.Nama);
            laporanbarang.SetParameterValue("paramalamat", data.Alamat);
            laporanbarang.SetParameterValue("paramtelepon", data.Telepon);
            crystalReportViewer1.ReportSource = laporanbarang;
            
        }
    }
}
