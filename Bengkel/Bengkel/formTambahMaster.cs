﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class formTambahMaster : Form
    {
        Karyawan username;
        public formTambahMaster(Karyawan username)
        {
            this.username = username;
            InitializeComponent();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog upload = new OpenFileDialog();
            upload.ShowDialog();
            upload.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            // lblfilename.Text = upload.FileName;
            lblfilename.Visible = true;
            //   MessageBox.Show(upload.SafeFileName);
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            File.Copy(upload.FileName, path + "\\FOTO\\" + upload.SafeFileName);
            lblfilename.Text = upload.SafeFileName;
        }

        private void lblfilename_Click(object sender, EventArgs e)
        {

        }
        public string get_IDKendaraan()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIdBengkel id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            kon.Close();
            return "MS" + id;

        }
        public void defaultform_load()
        {
            txtikaryawan.Text = get_IDKendaraan();

        }


        public void clear()
        {
            txtikaryawan.Text = "";
            txtnama.Text = "";
            txtalamat.Text = "";
            txttelepon.Text = "";
            lblfilename.Text = "";


        }

        private void btnsimpan_Click_1(object sender, EventArgs e)
        {

            BengkelEntities5 context = new BengkelEntities5();
            Master_Bengkel mas = new Master_Bengkel();

            mas.Id_Bengkel = get_IDKendaraan();
          //  mas.Nama = txtnama.Text;
          if(txtnama.Text=="")
            {
                MessageBox.Show("ISI NAMA DULU");
                txtnama.Focus();
            }
           // mas.Alamat = txtalamat.Text;
           if(txtalamat.Text=="")
            {
                MessageBox.Show("ISI ALAMAT DULU");
                txtalamat.Focus();
            }
          //  mas.Telepon = txttelepon.Text;
          if(txttelepon.Text=="")
            {
                MessageBox.Show("ISI TELEPON DULU");
                txttelepon.Focus();
            }
          //if(mas.logo== null)
          //  {
          //      MessageBox.Show("ISI dulu LOGO");
          //  }
          else
            {

                
                
                mas.Nama = txtnama.Text;
                mas.Alamat = txtalamat.Text;
                mas.Telepon = txttelepon.Text;
                mas.logo =  lblfilename.Text;
                context.Master_Bengkel.Add(mas);
                context.SaveChanges();
                MessageBox.Show("data Berhasil di input");
                clear();
            }
           
            


            //if(mas.logo == null)
            //{
            //    MessageBox.Show("ok");
            //}


           
        }

        private void txttelepon_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }
    }
    
    
}
