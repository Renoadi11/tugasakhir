﻿namespace Bengkel
{
    partial class utama
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnUtama = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnkluar = new System.Windows.Forms.Button();
            this.lbljam = new System.Windows.Forms.Label();
            this.lbltgl = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mASTERDATAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bARANGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sUPPLIERToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pENGGUNAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mEKANIKToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mASTERBENGKELToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pEMBELIANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pEMBELIANBARANGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rETURPEMBELIANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pENJUALANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bOOKINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rETURPENJUALANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.plapo = new System.Windows.Forms.ToolStripMenuItem();
            this.lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lAPORANSTOKBARANGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lAPORANPEMBELIANBARANGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lAPORANRETURPENJUALANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lAPORANRETURPEMBELIANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ps = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(157, 337);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(57, 20);
            this.lbl1.TabIndex = 9;
            this.lbl1.Text = "label1";
            this.lbl1.Visible = false;
            this.lbl1.Click += new System.EventHandler(this.lbl1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.label1.Font = new System.Drawing.Font("Microsoft New Tai Lue", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 337);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 21);
            this.label1.TabIndex = 10;
            this.label1.Text = "ADMINISTRATOR :";
            // 
            // pnUtama
            // 
            this.pnUtama.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnUtama.Location = new System.Drawing.Point(399, 55);
            this.pnUtama.Name = "pnUtama";
            this.pnUtama.Size = new System.Drawing.Size(356, 319);
            this.pnUtama.TabIndex = 11;
            this.pnUtama.Paint += new System.Windows.Forms.PaintEventHandler(this.pnUtama_Paint);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(9, 49);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(382, 269);
            this.dataGridView1.TabIndex = 12;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btnkluar
            // 
            this.btnkluar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnkluar.Location = new System.Drawing.Point(11, 361);
            this.btnkluar.Name = "btnkluar";
            this.btnkluar.Size = new System.Drawing.Size(75, 23);
            this.btnkluar.TabIndex = 13;
            this.btnkluar.Text = "KELUAR";
            this.btnkluar.UseVisualStyleBackColor = true;
            this.btnkluar.Click += new System.EventHandler(this.btnkluar_Click);
            // 
            // lbljam
            // 
            this.lbljam.AutoSize = true;
            this.lbljam.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbljam.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbljam.ForeColor = System.Drawing.Color.White;
            this.lbljam.Location = new System.Drawing.Point(413, 30);
            this.lbljam.Name = "lbljam";
            this.lbljam.Size = new System.Drawing.Size(29, 15);
            this.lbljam.TabIndex = 14;
            this.lbljam.Text = "Jam";
            // 
            // lbltgl
            // 
            this.lbltgl.AutoSize = true;
            this.lbltgl.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lbltgl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltgl.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbltgl.Location = new System.Drawing.Point(576, 31);
            this.lbltgl.Name = "lbltgl";
            this.lbltgl.Size = new System.Drawing.Size(49, 13);
            this.lbltgl.TabIndex = 15;
            this.lbltgl.Text = "tanggal";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(185, 396);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(440, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "-----TERIMAKASIH SUDAH MENGGUNAKAN APLIKASI BENGKEL-----";
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(254, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "PEMBERITAHUAN BARANG HABIS";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mASTERDATAToolStripMenuItem,
            this.pEMBELIANToolStripMenuItem,
            this.pENJUALANToolStripMenuItem,
            this.plapo,
            this.ps});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(767, 24);
            this.menuStrip1.TabIndex = 18;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mASTERDATAToolStripMenuItem
            // 
            this.mASTERDATAToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bARANGToolStripMenuItem,
            this.sUPPLIERToolStripMenuItem,
            this.pENGGUNAToolStripMenuItem,
            this.mEKANIKToolStripMenuItem,
            this.mASTERBENGKELToolStripMenuItem});
            this.mASTERDATAToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mASTERDATAToolStripMenuItem.Name = "mASTERDATAToolStripMenuItem";
            this.mASTERDATAToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.mASTERDATAToolStripMenuItem.Text = "MASTER DATA";
            this.mASTERDATAToolStripMenuItem.Click += new System.EventHandler(this.mASTERDATAToolStripMenuItem_Click);
            // 
            // bARANGToolStripMenuItem
            // 
            this.bARANGToolStripMenuItem.Name = "bARANGToolStripMenuItem";
            this.bARANGToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.bARANGToolStripMenuItem.Text = "BARANG";
            this.bARANGToolStripMenuItem.Click += new System.EventHandler(this.bARANGToolStripMenuItem_Click);
            // 
            // sUPPLIERToolStripMenuItem
            // 
            this.sUPPLIERToolStripMenuItem.Name = "sUPPLIERToolStripMenuItem";
            this.sUPPLIERToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.sUPPLIERToolStripMenuItem.Text = "SUPPLIER";
            this.sUPPLIERToolStripMenuItem.Click += new System.EventHandler(this.sUPPLIERToolStripMenuItem_Click);
            // 
            // pENGGUNAToolStripMenuItem
            // 
            this.pENGGUNAToolStripMenuItem.Name = "pENGGUNAToolStripMenuItem";
            this.pENGGUNAToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.pENGGUNAToolStripMenuItem.Text = "PENGGUNA";
            this.pENGGUNAToolStripMenuItem.Click += new System.EventHandler(this.pENGGUNAToolStripMenuItem_Click);
            // 
            // mEKANIKToolStripMenuItem
            // 
            this.mEKANIKToolStripMenuItem.Name = "mEKANIKToolStripMenuItem";
            this.mEKANIKToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.mEKANIKToolStripMenuItem.Text = "MEKANIK";
            this.mEKANIKToolStripMenuItem.Click += new System.EventHandler(this.mEKANIKToolStripMenuItem_Click);
            // 
            // mASTERBENGKELToolStripMenuItem
            // 
            this.mASTERBENGKELToolStripMenuItem.Name = "mASTERBENGKELToolStripMenuItem";
            this.mASTERBENGKELToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.mASTERBENGKELToolStripMenuItem.Text = "MASTER BENGKEL";
            this.mASTERBENGKELToolStripMenuItem.Click += new System.EventHandler(this.mASTERBENGKELToolStripMenuItem_Click);
            // 
            // pEMBELIANToolStripMenuItem
            // 
            this.pEMBELIANToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pEMBELIANBARANGToolStripMenuItem,
            this.rETURPEMBELIANToolStripMenuItem});
            this.pEMBELIANToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pEMBELIANToolStripMenuItem.Name = "pEMBELIANToolStripMenuItem";
            this.pEMBELIANToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.pEMBELIANToolStripMenuItem.Text = "PEMBELIAN";
            this.pEMBELIANToolStripMenuItem.Click += new System.EventHandler(this.pEMBELIANToolStripMenuItem_Click);
            // 
            // pEMBELIANBARANGToolStripMenuItem
            // 
            this.pEMBELIANBARANGToolStripMenuItem.Name = "pEMBELIANBARANGToolStripMenuItem";
            this.pEMBELIANBARANGToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.pEMBELIANBARANGToolStripMenuItem.Text = "PEMBELIAN BARANG";
            this.pEMBELIANBARANGToolStripMenuItem.Click += new System.EventHandler(this.pEMBELIANBARANGToolStripMenuItem_Click);
            // 
            // rETURPEMBELIANToolStripMenuItem
            // 
            this.rETURPEMBELIANToolStripMenuItem.Name = "rETURPEMBELIANToolStripMenuItem";
            this.rETURPEMBELIANToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.rETURPEMBELIANToolStripMenuItem.Text = "RETUR PEMBELIAN";
            this.rETURPEMBELIANToolStripMenuItem.Click += new System.EventHandler(this.rETURPEMBELIANToolStripMenuItem_Click);
            // 
            // pENJUALANToolStripMenuItem
            // 
            this.pENJUALANToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.bOOKINGToolStripMenuItem,
            this.rETURPENJUALANToolStripMenuItem});
            this.pENJUALANToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pENJUALANToolStripMenuItem.Name = "pENJUALANToolStripMenuItem";
            this.pENJUALANToolStripMenuItem.Size = new System.Drawing.Size(86, 20);
            this.pENJUALANToolStripMenuItem.Text = "PENJUALAN";
            this.pENJUALANToolStripMenuItem.Click += new System.EventHandler(this.pENJUALANToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(239, 22);
            this.toolStripMenuItem1.Text = "PENJUALAN SERVIS/BARANG";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // bOOKINGToolStripMenuItem
            // 
            this.bOOKINGToolStripMenuItem.Name = "bOOKINGToolStripMenuItem";
            this.bOOKINGToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.bOOKINGToolStripMenuItem.Text = "BOOKING";
            this.bOOKINGToolStripMenuItem.Click += new System.EventHandler(this.bOOKINGToolStripMenuItem_Click);
            // 
            // rETURPENJUALANToolStripMenuItem
            // 
            this.rETURPENJUALANToolStripMenuItem.Name = "rETURPENJUALANToolStripMenuItem";
            this.rETURPENJUALANToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.rETURPENJUALANToolStripMenuItem.Text = "RETUR PENJUALAN";
            this.rETURPENJUALANToolStripMenuItem.Click += new System.EventHandler(this.rETURPENJUALANToolStripMenuItem_Click);
            // 
            // plapo
            // 
            this.plapo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem,
            this.lAPORANSTOKBARANGToolStripMenuItem,
            this.lAPORANPEMBELIANBARANGToolStripMenuItem,
            this.lAPORANRETURPENJUALANToolStripMenuItem,
            this.lAPORANRETURPEMBELIANToolStripMenuItem});
            this.plapo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plapo.Name = "plapo";
            this.plapo.Size = new System.Drawing.Size(74, 20);
            this.plapo.Text = "LAPORAN";
            this.plapo.Click += new System.EventHandler(this.lAPORANToolStripMenuItem_Click);
            // 
            // lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem
            // 
            this.lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem.Name = "lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem";
            this.lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem.Text = "LAPORAN PENJUALAN BARANG DAN SERVIS";
            this.lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem.Click += new System.EventHandler(this.lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem_Click);
            // 
            // lAPORANSTOKBARANGToolStripMenuItem
            // 
            this.lAPORANSTOKBARANGToolStripMenuItem.Name = "lAPORANSTOKBARANGToolStripMenuItem";
            this.lAPORANSTOKBARANGToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.lAPORANSTOKBARANGToolStripMenuItem.Text = "LAPORAN STOK BARANG";
            this.lAPORANSTOKBARANGToolStripMenuItem.Click += new System.EventHandler(this.lAPORANSTOKBARANGToolStripMenuItem_Click);
            // 
            // lAPORANPEMBELIANBARANGToolStripMenuItem
            // 
            this.lAPORANPEMBELIANBARANGToolStripMenuItem.Name = "lAPORANPEMBELIANBARANGToolStripMenuItem";
            this.lAPORANPEMBELIANBARANGToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.lAPORANPEMBELIANBARANGToolStripMenuItem.Text = "LAPORAN PEMBELIAN BARANG";
            this.lAPORANPEMBELIANBARANGToolStripMenuItem.Click += new System.EventHandler(this.lAPORANPEMBELIANBARANGToolStripMenuItem_Click);
            // 
            // lAPORANRETURPENJUALANToolStripMenuItem
            // 
            this.lAPORANRETURPENJUALANToolStripMenuItem.Name = "lAPORANRETURPENJUALANToolStripMenuItem";
            this.lAPORANRETURPENJUALANToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.lAPORANRETURPENJUALANToolStripMenuItem.Text = "LAPORAN RETUR PENJUALAN";
            this.lAPORANRETURPENJUALANToolStripMenuItem.Click += new System.EventHandler(this.lAPORANRETURPENJUALANToolStripMenuItem_Click);
            // 
            // lAPORANRETURPEMBELIANToolStripMenuItem
            // 
            this.lAPORANRETURPEMBELIANToolStripMenuItem.Name = "lAPORANRETURPEMBELIANToolStripMenuItem";
            this.lAPORANRETURPEMBELIANToolStripMenuItem.Size = new System.Drawing.Size(324, 22);
            this.lAPORANRETURPEMBELIANToolStripMenuItem.Text = "LAPORAN RETUR PEMBELIAN";
            this.lAPORANRETURPEMBELIANToolStripMenuItem.Click += new System.EventHandler(this.lAPORANRETURPEMBELIANToolStripMenuItem_Click);
            // 
            // ps
            // 
            this.ps.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ps.Name = "ps";
            this.ps.Size = new System.Drawing.Size(98, 20);
            this.ps.Text = "PENGATURAN";
            this.ps.Click += new System.EventHandler(this.pENGATURANToolStripMenuItem_Click);
            // 
            // utama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(767, 420);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lbltgl);
            this.Controls.Add(this.lbljam);
            this.Controls.Add(this.btnkluar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.pnUtama);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "utama";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Utama";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.utama_FormClosing);
            this.Load += new System.EventHandler(this.utama_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnUtama;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnkluar;
        private System.Windows.Forms.Label lbljam;
        private System.Windows.Forms.Label lbltgl;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mASTERDATAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pEMBELIANToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pENJUALANToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem plapo;
        private System.Windows.Forms.ToolStripMenuItem ps;
        private System.Windows.Forms.ToolStripMenuItem bARANGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sUPPLIERToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pENGGUNAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mEKANIKToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mASTERBENGKELToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pEMBELIANBARANGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rETURPEMBELIANToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem bOOKINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rETURPENJUALANToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lAPORANSTOKBARANGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lAPORANPEMBELIANBARANGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lAPORANRETURPENJUALANToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lAPORANRETURPEMBELIANToolStripMenuItem;
    }
}