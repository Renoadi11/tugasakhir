﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace Bengkel
{
    public partial class Formlaporanjual : Form
    {
        Karyawan username;
        Nota_jual_barang nota;
        public Formlaporanjual(Karyawan username, Nota_jual_barang nota)
        {
            this.username = username;
            this.nota = nota;
            InitializeComponent();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            BengkelEntities5 contex = new BengkelEntities5();
            Master_Bengkel data = contex.Master_Bengkel.Where(c => c.Id_Bengkel == username.Bengkel_id).FirstOrDefault();
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            string filename = path + "\\FOTO\\" + data.logo;

            CrystalReport6 laporanbarang = new CrystalReport6();
            laporanbarang.SetParameterValue("@idnota",nota.Id_nota_Jual);
            laporanbarang.SetParameterValue("paramtaggl", nota.Tanggal_Jual);
            laporanbarang.SetParameterValue("paramadmin", username.Nama);
            laporanbarang.SetParameterValue("parambengkel", data.Nama);
            laporanbarang.SetParameterValue("paramalmat", data.Alamat);
            laporanbarang.SetParameterValue("paramtelepon", data.Telepon);
            laporanbarang.SetParameterValue("paramtotal", nota.Total);
            laporanbarang.SetParameterValue("parambayar", nota.jumlah_bayar);
            laporanbarang.SetParameterValue("paramkemabli", nota.kembalian);
            laporanbarang.SetParameterValue("urlupload",filename);
            crystalReportViewer1.ReportSource = laporanbarang;
           // crystalReportViewer1.DisplayGroupTree = false;
        }
    }
}
