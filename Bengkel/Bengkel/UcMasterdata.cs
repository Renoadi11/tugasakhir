﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class UcMasterdata : UserControl
    {
        utama ut;
        Karyawan ky;
        public UcMasterdata(utama ut, Karyawan ky)
        {
            this.ut = ut;
            this.ky = ky;
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Barangg tampil = new Barangg(ky, ut);
            ut.Hide();
            tampil.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Suplierr tampil = new Suplierr(ky, ut);
            ut.Hide();
            tampil.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Karyawann tampil = new Karyawann(ky, ut);
            ut.Hide();
            tampil.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Masterbengkel tam = new Masterbengkel(ky, ut);
            ut.Hide();
            tam.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Mekanikk mek = new Mekanikk(ky, ut);
            ut.Hide();
            mek.Show();
                
        }
    }
    
}
