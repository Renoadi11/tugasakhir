﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;


namespace Bengkel
{
    public partial class upkeahliann : Form
    {
        
        Karyawan username;
        string id;
        Keahlian keh;
        BengkelEntities5 upp = new BengkelEntities5();
        public upkeahliann(string id, Karyawan username)
        {
            this.username = username;
            InitializeComponent();
            this.id = id;
            load();

        }
        public void load()
        {
            var data1 = (from x in upp.Keahlians where x.Id_Keahlian == id select x).FirstOrDefault<Keahlian>();
            txtnamakeahlian.Text = data1.Nama_Keahlian;

            keh= data1;
        }

        private void btnSimpan_Click(object sender, EventArgs e)
        {
            keh.Nama_Keahlian = txtnamakeahlian.Text;

            upp.SaveChanges();
            MessageBox.Show("Data Berhasil Diubah...");
        }
    }
}
