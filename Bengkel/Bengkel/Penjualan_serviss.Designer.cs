﻿namespace Bengkel
{
    partial class Penjualan_serviss
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbltgl = new System.Windows.Forms.Label();
            this.lblnota = new System.Windows.Forms.Label();
            this.lbladmin = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txttotal = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.txtpottongan = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cboservis = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtpot = new System.Windows.Forms.TextBox();
            this.cbobarang = new System.Windows.Forms.ComboBox();
            this.txttottal = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.button4 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtjumlah = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbomek = new System.Windows.Forms.ComboBox();
            this.cbostatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtsisa = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtsub = new System.Windows.Forms.TextBox();
            this.txtketera = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtbayar = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.ckbarang = new System.Windows.Forms.CheckBox();
            this.ckservis = new System.Windows.Forms.CheckBox();
            this.btndiskon = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblbooking = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.advancedDataGridView1 = new Zuby.ADGV.AdvancedDataGridView();
            this.idnotaJualDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tanggalJualDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bengkelidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vwnotajualBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bengkelDataSet4 = new Bengkel.BengkelDataSet4();
            this.bookingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bengkelDataSet = new Bengkel.BengkelDataSet();
            this.nota_jual_barangTableAdapter = new Bengkel.BengkelDataSet1TableAdapters.Nota_jual_barangTableAdapter();
            this.bookingTableAdapter = new Bengkel.BengkelDataSetTableAdapters.BookingTableAdapter();
            this.vw_notajualTableAdapter = new Bengkel.BengkelDataSet4TableAdapters.vw_notajualTableAdapter();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advancedDataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwnotajualBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bengkelDataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bengkelDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.groupBox1.Controls.Add(this.lbltgl);
            this.groupBox1.Controls.Add(this.lblnota);
            this.groupBox1.Controls.Add(this.lbladmin);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(337, 99);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // lbltgl
            // 
            this.lbltgl.AutoSize = true;
            this.lbltgl.Location = new System.Drawing.Point(115, 45);
            this.lbltgl.Name = "lbltgl";
            this.lbltgl.Size = new System.Drawing.Size(41, 13);
            this.lbltgl.TabIndex = 13;
            this.lbltgl.Text = "label5";
            // 
            // lblnota
            // 
            this.lblnota.AutoSize = true;
            this.lblnota.Location = new System.Drawing.Point(118, 15);
            this.lblnota.Name = "lblnota";
            this.lblnota.Size = new System.Drawing.Size(48, 13);
            this.lblnota.TabIndex = 12;
            this.lblnota.Text = "label32";
            // 
            // lbladmin
            // 
            this.lbladmin.AutoSize = true;
            this.lbladmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbladmin.Location = new System.Drawing.Point(115, 75);
            this.lbladmin.Name = "lbladmin";
            this.lbladmin.Size = new System.Drawing.Size(48, 13);
            this.lbladmin.TabIndex = 11;
            this.lbladmin.Text = "label29";
            this.lbladmin.Click += new System.EventHandler(this.lbladmin_Click);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(55, 75);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 10;
            this.label28.Text = "ADMIN :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "TANGGAL :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "NO TRANSAKSI :";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.textBox4);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.txttotal);
            this.groupBox4.Controls.Add(this.dataGridView1);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.button3);
            this.groupBox4.Controls.Add(this.txtpottongan);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.cboservis);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Location = new System.Drawing.Point(12, 131);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(830, 244);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(438, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(12, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(38, 19);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "*";
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(520, 16);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(29, 20);
            this.textBox4.TabIndex = 9;
            this.textBox4.Text = "1";
            this.textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(450, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "JUMLAH :";
            // 
            // txttotal
            // 
            this.txttotal.Enabled = false;
            this.txttotal.Location = new System.Drawing.Point(681, 213);
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new System.Drawing.Size(116, 20);
            this.txttotal.TabIndex = 7;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(7, 71);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(800, 133);
            this.dataGridView1.TabIndex = 6;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(596, 216);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(84, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "SUB TOTAL :";
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(779, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = ">";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Location = new System.Drawing.Point(7, 206);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(128, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "HAPUS ITEM";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtpottongan
            // 
            this.txtpottongan.Enabled = false;
            this.txtpottongan.Location = new System.Drawing.Point(681, 16);
            this.txtpottongan.Name = "txtpottongan";
            this.txtpottongan.Size = new System.Drawing.Size(76, 20);
            this.txtpottongan.TabIndex = 4;
            this.txtpottongan.Text = "0";
            this.txtpottongan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtpottongan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpottongan_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(591, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "POTONGAN :";
            // 
            // cboservis
            // 
            this.cboservis.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboservis.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboservis.Enabled = false;
            this.cboservis.FormattingEnabled = true;
            this.cboservis.Location = new System.Drawing.Point(111, 14);
            this.cboservis.Name = "cboservis";
            this.cboservis.Size = new System.Drawing.Size(317, 21);
            this.cboservis.TabIndex = 1;
            this.cboservis.SelectedIndexChanged += new System.EventHandler(this.cboservis_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(46, 19);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(60, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "SERVIS :";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.txtpot);
            this.groupBox5.Controls.Add(this.cbobarang);
            this.groupBox5.Controls.Add(this.txttottal);
            this.groupBox5.Controls.Add(this.label20);
            this.groupBox5.Controls.Add(this.button6);
            this.groupBox5.Controls.Add(this.button5);
            this.groupBox5.Controls.Add(this.dataGridView2);
            this.groupBox5.Controls.Add(this.button4);
            this.groupBox5.Controls.Add(this.label19);
            this.groupBox5.Controls.Add(this.txtjumlah);
            this.groupBox5.Controls.Add(this.label18);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Location = new System.Drawing.Point(12, 381);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(828, 261);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(463, 14);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(12, 13);
            this.label23.TabIndex = 30;
            this.label23.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(17, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(12, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "*";
            // 
            // txtpot
            // 
            this.txtpot.Enabled = false;
            this.txtpot.Location = new System.Drawing.Point(691, 16);
            this.txtpot.Name = "txtpot";
            this.txtpot.Size = new System.Drawing.Size(42, 20);
            this.txtpot.TabIndex = 14;
            this.txtpot.Text = "0";
            this.txtpot.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtpot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtpot_KeyPress);
            // 
            // cbobarang
            // 
            this.cbobarang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cbobarang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbobarang.Enabled = false;
            this.cbobarang.FormattingEnabled = true;
            this.cbobarang.Location = new System.Drawing.Point(97, 12);
            this.cbobarang.Name = "cbobarang";
            this.cbobarang.Size = new System.Drawing.Size(341, 21);
            this.cbobarang.TabIndex = 13;
            // 
            // txttottal
            // 
            this.txttottal.Enabled = false;
            this.txttottal.Location = new System.Drawing.Point(691, 219);
            this.txttottal.Name = "txttottal";
            this.txttottal.Size = new System.Drawing.Size(116, 20);
            this.txttottal.TabIndex = 12;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(609, 222);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "SUB TOTAL :";
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Location = new System.Drawing.Point(174, 222);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(87, 23);
            this.button6.TabIndex = 10;
            this.button6.Text = "HAPUS";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.Location = new System.Drawing.Point(7, 222);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(149, 23);
            this.button5.TabIndex = 9;
            this.button5.Text = "SUNTING BARANG";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(7, 53);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(800, 163);
            this.dataGridView2.TabIndex = 8;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(770, 10);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(37, 23);
            this.button4.TabIndex = 7;
            this.button4.Text = ">";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(603, 19);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(84, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "POTONGAN :";
            // 
            // txtjumlah
            // 
            this.txtjumlah.Enabled = false;
            this.txtjumlah.Location = new System.Drawing.Point(540, 14);
            this.txtjumlah.Name = "txtjumlah";
            this.txtjumlah.Size = new System.Drawing.Size(42, 20);
            this.txtjumlah.TabIndex = 4;
            this.txtjumlah.Text = "1";
            this.txtjumlah.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtjumlah.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtjumlah_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(477, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(64, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "JUMLAH :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(25, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "BARANG :";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.cbomek);
            this.groupBox6.Controls.Add(this.cbostatus);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.txtsisa);
            this.groupBox6.Controls.Add(this.label27);
            this.groupBox6.Controls.Add(this.txtsub);
            this.groupBox6.Controls.Add(this.txtketera);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.txtbayar);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Location = new System.Drawing.Point(848, 131);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(425, 443);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " PEMBAYARAN";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(15, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 13);
            this.label9.TabIndex = 27;
            this.label9.Text = "*";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(25, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 13);
            this.label8.TabIndex = 26;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "MEKANIK :";
            // 
            // cbomek
            // 
            this.cbomek.Enabled = false;
            this.cbomek.FormattingEnabled = true;
            this.cbomek.Location = new System.Drawing.Point(98, 16);
            this.cbomek.Name = "cbomek";
            this.cbomek.Size = new System.Drawing.Size(121, 21);
            this.cbomek.TabIndex = 12;
            // 
            // cbostatus
            // 
            this.cbostatus.FormattingEnabled = true;
            this.cbostatus.Location = new System.Drawing.Point(98, 45);
            this.cbostatus.Name = "cbostatus";
            this.cbostatus.Size = new System.Drawing.Size(103, 21);
            this.cbostatus.TabIndex = 9;
            this.cbostatus.Visible = false;
            this.cbostatus.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(-2, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "KETERANGAN :";
            this.label3.Click += new System.EventHandler(this.label3_Click_1);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(32, 51);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "STATUS :";
            this.label21.Visible = false;
            // 
            // txtsisa
            // 
            this.txtsisa.Enabled = false;
            this.txtsisa.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsisa.Location = new System.Drawing.Point(98, 370);
            this.txtsisa.Multiline = true;
            this.txtsisa.Name = "txtsisa";
            this.txtsisa.Size = new System.Drawing.Size(279, 48);
            this.txtsisa.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(38, 376);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(54, 20);
            this.label27.TabIndex = 10;
            this.label27.Text = "Sisa :";
            // 
            // txtsub
            // 
            this.txtsub.Enabled = false;
            this.txtsub.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsub.Location = new System.Drawing.Point(98, 289);
            this.txtsub.Multiline = true;
            this.txtsub.Name = "txtsub";
            this.txtsub.Size = new System.Drawing.Size(279, 55);
            this.txtsub.TabIndex = 9;
            // 
            // txtketera
            // 
            this.txtketera.Location = new System.Drawing.Point(98, 85);
            this.txtketera.Multiline = true;
            this.txtketera.Name = "txtketera";
            this.txtketera.Size = new System.Drawing.Size(279, 108);
            this.txtketera.TabIndex = 3;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(33, 309);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(59, 20);
            this.label26.TabIndex = 8;
            this.label26.Text = "Total :";
            // 
            // txtbayar
            // 
            this.txtbayar.Enabled = false;
            this.txtbayar.Location = new System.Drawing.Point(98, 238);
            this.txtbayar.Multiline = true;
            this.txtbayar.Name = "txtbayar";
            this.txtbayar.Size = new System.Drawing.Size(279, 28);
            this.txtbayar.TabIndex = 7;
            this.txtbayar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtbayar_KeyPress);
            this.txtbayar.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtbayar_KeyUp);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(34, 251);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(56, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "BAYAR :";
            // 
            // button9
            // 
            this.button9.Enabled = false;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(1061, 579);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(218, 31);
            this.button9.TabIndex = 9;
            this.button9.Text = "SIMPAN+CETAK NOTA";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.groupBox7.Controls.Add(this.ckbarang);
            this.groupBox7.Controls.Add(this.ckservis);
            this.groupBox7.Location = new System.Drawing.Point(396, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(209, 79);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "JENIS PENJUALAN";
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // ckbarang
            // 
            this.ckbarang.AutoSize = true;
            this.ckbarang.Location = new System.Drawing.Point(49, 46);
            this.ckbarang.Name = "ckbarang";
            this.ckbarang.Size = new System.Drawing.Size(77, 17);
            this.ckbarang.TabIndex = 1;
            this.ckbarang.Text = "BARANG";
            this.ckbarang.UseVisualStyleBackColor = true;
            this.ckbarang.CheckedChanged += new System.EventHandler(this.ckbarang_CheckedChanged);
            // 
            // ckservis
            // 
            this.ckservis.AutoSize = true;
            this.ckservis.Location = new System.Drawing.Point(49, 23);
            this.ckservis.Name = "ckservis";
            this.ckservis.Size = new System.Drawing.Size(71, 17);
            this.ckservis.TabIndex = 0;
            this.ckservis.Text = "SERVIS";
            this.ckservis.UseVisualStyleBackColor = true;
            this.ckservis.CheckedChanged += new System.EventHandler(this.ckservis_CheckedChanged);
            // 
            // btndiskon
            // 
            this.btndiskon.Location = new System.Drawing.Point(611, 37);
            this.btndiskon.Name = "btndiskon";
            this.btndiskon.Size = new System.Drawing.Size(118, 23);
            this.btndiskon.TabIndex = 12;
            this.btndiskon.Text = "Daftar Booking";
            this.btndiskon.UseVisualStyleBackColor = true;
            this.btndiskon.Click += new System.EventHandler(this.button2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(656, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 13;
            // 
            // lblbooking
            // 
            this.lblbooking.AutoSize = true;
            this.lblbooking.Location = new System.Drawing.Point(735, 42);
            this.lblbooking.Name = "lblbooking";
            this.lblbooking.Size = new System.Drawing.Size(41, 13);
            this.lblbooking.TabIndex = 14;
            this.lblbooking.Text = "label6";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(408, 113);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(509, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "--TELITI SEBELUM MELAKUKAN TRANSAKSI PENJUALAN MOHON DI CEK KEMBALI-- ";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // advancedDataGridView1
            // 
            this.advancedDataGridView1.AllowUserToAddRows = false;
            this.advancedDataGridView1.AllowUserToDeleteRows = false;
            this.advancedDataGridView1.AutoGenerateColumns = false;
            this.advancedDataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.advancedDataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idnotaJualDataGridViewTextBoxColumn,
            this.tanggalJualDataGridViewTextBoxColumn,
            this.bengkelidDataGridViewTextBoxColumn});
            this.advancedDataGridView1.DataSource = this.vwnotajualBindingSource;
            this.advancedDataGridView1.FilterAndSortEnabled = true;
            this.advancedDataGridView1.Location = new System.Drawing.Point(819, 12);
            this.advancedDataGridView1.Name = "advancedDataGridView1";
            this.advancedDataGridView1.ReadOnly = true;
            this.advancedDataGridView1.Size = new System.Drawing.Size(454, 88);
            this.advancedDataGridView1.TabIndex = 16;
            this.advancedDataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.advancedDataGridView1_CellContentClick);
            // 
            // idnotaJualDataGridViewTextBoxColumn
            // 
            this.idnotaJualDataGridViewTextBoxColumn.DataPropertyName = "Id_nota_Jual";
            this.idnotaJualDataGridViewTextBoxColumn.HeaderText = "Id_nota_Jual";
            this.idnotaJualDataGridViewTextBoxColumn.MinimumWidth = 22;
            this.idnotaJualDataGridViewTextBoxColumn.Name = "idnotaJualDataGridViewTextBoxColumn";
            this.idnotaJualDataGridViewTextBoxColumn.ReadOnly = true;
            this.idnotaJualDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // tanggalJualDataGridViewTextBoxColumn
            // 
            this.tanggalJualDataGridViewTextBoxColumn.DataPropertyName = "Tanggal_Jual";
            this.tanggalJualDataGridViewTextBoxColumn.HeaderText = "Tanggal_Jual";
            this.tanggalJualDataGridViewTextBoxColumn.MinimumWidth = 22;
            this.tanggalJualDataGridViewTextBoxColumn.Name = "tanggalJualDataGridViewTextBoxColumn";
            this.tanggalJualDataGridViewTextBoxColumn.ReadOnly = true;
            this.tanggalJualDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // bengkelidDataGridViewTextBoxColumn
            // 
            this.bengkelidDataGridViewTextBoxColumn.DataPropertyName = "Bengkel_id";
            this.bengkelidDataGridViewTextBoxColumn.HeaderText = "Bengkel_id";
            this.bengkelidDataGridViewTextBoxColumn.MinimumWidth = 22;
            this.bengkelidDataGridViewTextBoxColumn.Name = "bengkelidDataGridViewTextBoxColumn";
            this.bengkelidDataGridViewTextBoxColumn.ReadOnly = true;
            this.bengkelidDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic;
            // 
            // vwnotajualBindingSource
            // 
            this.vwnotajualBindingSource.DataMember = "vw_notajual";
            this.vwnotajualBindingSource.DataSource = this.bengkelDataSet4;
            // 
            // bengkelDataSet4
            // 
            this.bengkelDataSet4.DataSetName = "BengkelDataSet4";
            this.bengkelDataSet4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bengkelDataSet
            // 
            this.bengkelDataSet.DataSetName = "BengkelDataSet";
            this.bengkelDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nota_jual_barangTableAdapter
            // 
            this.nota_jual_barangTableAdapter.ClearBeforeFill = true;
            // 
            // bookingTableAdapter
            // 
            this.bookingTableAdapter.ClearBeforeFill = true;
            // 
            // vw_notajualTableAdapter
            // 
            this.vw_notajualTableAdapter.ClearBeforeFill = true;
            // 
            // Penjualan_serviss
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSkyBlue;
            this.ClientSize = new System.Drawing.Size(1299, 672);
            this.Controls.Add(this.advancedDataGridView1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblbooking);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btndiskon);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Penjualan_serviss";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PenjualanServis/Barang";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Penjualan_serviss_FormClosing);
            this.Load += new System.EventHandler(this.Penjualan_serviss_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.advancedDataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwnotajualBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bengkelDataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bookingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bengkelDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtpottongan;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cboservis;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txttotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txttottal;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtjumlah;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtsisa;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtsub;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtbayar;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbladmin;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cbobarang;
        private System.Windows.Forms.TextBox txtpot;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox ckbarang;
        private System.Windows.Forms.CheckBox ckservis;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblnota;
        public System.Windows.Forms.Label lbltgl;
        public System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.ComboBox cbostatus;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button btndiskon;
        private System.Windows.Forms.Label lblbooking;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cbomek;
        public System.Windows.Forms.TextBox txtketera;
        private Zuby.ADGV.AdvancedDataGridView advancedDataGridView1;
        private BengkelDataSet bengkelDataSet;
        private System.Windows.Forms.BindingSource bookingBindingSource;
        private BengkelDataSetTableAdapters.BookingTableAdapter bookingTableAdapter;
        private BengkelDataSet1TableAdapters.Nota_jual_barangTableAdapter nota_jual_barangTableAdapter;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private BengkelDataSet4 bengkelDataSet4;
        private System.Windows.Forms.BindingSource vwnotajualBindingSource;
        private BengkelDataSet4TableAdapters.vw_notajualTableAdapter vw_notajualTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idnotaJualDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tanggalJualDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn bengkelidDataGridViewTextBoxColumn;
    }
}