﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;
using System.Configuration;

namespace Bengkel
{
    public partial class Bookingg : Form
    {
        Karyawan username;
        utama ut;
        modelmekanik Mek;
        public List<ModelBarangNota> datanota = new List<ModelBarangNota>();
        public Bookingg(Karyawan username, utama ut)
        {
          
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            lbladmin.Text = username.Nama;
            defaultform_load();


        }
        public string get_idbooking()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select CONCAT(YEAR( GETDATE()),RIGHT('00' + CONVERT(VARCHAR(12),  MONTH(GETDATE())), 2),RIGHT('0000' + CONVERT(VARCHAR(12), next value for Getidbooking), 4)) id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "BKG" + id;
        }
        public void defaultform_load()
        {

            lblbooking.Text = get_idbooking();
            //dtpiker.Text=DateTime.Now.ToString();
            

            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Barang, Nama_Barang, Jenis_Barang, Harga_Satuan, Harga_Beli, No_rak_barang, Stok_Dimiliki, Stok_minimal, Status_barang, penambahan_Stok from Barang where Bengkel_id = @id and Hapus=0;";
            string sql2 = "select Id_Mekanik,nama,Alamat,telepon from Mekanik where bengkel_id =@id";
            List<ModelBarrang> data = kon.Query<ModelBarrang>(sql, new { id = username.Bengkel_id }).ToList();
            List<modelmekanik> data2 = kon.Query<modelmekanik>(sql2, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            foreach (var item in data)
            {
                cbobarser.Items.Add(item);
            }
            foreach (var item in data2)
            {
                cbomekanik.Items.Add(item);
            }
        }
        private void groupBox1_Enter(object sender, EventArgs e)
        {
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void Transaksi_Enter(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbomekanik.SelectedIndex > -1)
            {
                modelmekanik mekanik = (modelmekanik)cbomekanik.SelectedItem;
                lblmekanik.Text = mekanik.Nama.ToString();
               
            }
        }


        private void btnsimpan_Click(object sender, EventArgs e)
        {
            try
            {
                BengkelEntities5 context = new BengkelEntities5();
                Booking boking = new Booking();
              
             
                if(txtnama.Text=="")
                {
                    MessageBox.Show("ISI NAMA TERLEBIH DAHULU");
                    txtnama.Focus();
                }
                if(txtalmt.Text=="")
                {
                    MessageBox.Show("ISI ALAMAT TERLEBIH DAHULU");
                    txtalmt.Focus();
                }
               if(txttlp.Text=="")
                {
                    MessageBox.Show("ISI TELEPON TERLEBIH DAHULU");
                    txttlp.Focus();
                }
               if(txtketerangan.Text=="")
                {
                    MessageBox.Show("ISI KETERANGAN TERLEBIH DAHULU");
                    txtketerangan.Focus();
                }
               if(cbomekanik.SelectedIndex==-1)
                {
                    MessageBox.Show("isi DULU NAMA MEKANIK");
                }
               if(txtjml.Text =="")
                {
                    MessageBox.Show("ISI JUMLAH");
                    txtjml.Focus();
                }
                else
                {
                    boking.Id_Booking = get_idbooking();
                    Mek = (modelmekanik)cbomekanik.SelectedItem;
                    boking.mekanik_id = Mek.Id_Mekanik;
                    boking.Nama = txtnama.Text;
                    boking.Alamat = txtalmt.Text;
                    boking.Telpon = txttlp.Text;
                    boking.Keterangan = txtketerangan.Text;
                    boking.Tanggal_booking = dtpiker.Value;
                    boking.Status_id = 1;
                    boking.Karyawan_Id = username.Id_Karyawan;
                    boking.bengekl_id = username.Bengkel_id;
                 
                    context.Bookings.Add(boking);
                    context.SaveChanges();
                    MessageBox.Show("Data Berhasil di Simpan");
                }
                foreach (var item in datanota)
                {
                    Detail_booking det = new Detail_booking();
                    det.Booiking_id = boking.Id_Booking;
                    det.id_barang = item.IdBarang;
                    det.nama_barang = item.NamaBarang;
                    det.harga = item.Harga;
                    det.jumlah = item.Jumlah;
                    det.potongan = item.Potongan;
                    det.sub_total = item.SubTotal;
                    context.Detail_booking.Add(det);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Isi Sesuai Dengan Booking Pelanggan");
            } 
        }

        private void btntmbh_Click(object sender, EventArgs e)
        {
            ModelBarrang barang = (ModelBarrang)cbobarser.SelectedItem;
            

            ModelBarangNota item = new ModelBarangNota();
            if(cbobarser.SelectedIndex==-1)
            {
                MessageBox.Show("Pilih Barang Terlebih Dahulu");
            }
            else
            {
                item.IdBarang = barang.Id_Barang;
                item.Jumlah = int.Parse(txtjml.Text);
                item.NamaBarang = barang.Nama_Barang;
                item.Potongan = int.Parse(txtpotongan.Text);
                item.Harga = barang.Harga_Satuan;

                datanota.Add(item);
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = datanota;
                txtsub.Text = datanota.Sum(i => i.SubTotal).ToString();
            }
           


        }

        private void Bookingg_Load(object sender, EventArgs e)
        {

        }

        private void txttlp_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void Bookingg_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }
    }
}
