﻿namespace Bengkel
{
    partial class Lpembelian
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnok = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnok
            // 
            this.btnok.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Location = new System.Drawing.Point(40, 98);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(213, 23);
            this.btnok.TabIndex = 0;
            this.btnok.Text = "LAPORAN PEMBELIAN";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // Lpembelian
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnok);
            this.Name = "Lpembelian";
            this.Text = "Lpembelian";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Lpembelian_FormClosing);
            this.Load += new System.EventHandler(this.Lpembelian_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnok;
    }
}