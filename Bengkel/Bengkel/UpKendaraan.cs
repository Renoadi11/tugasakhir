﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class UpKendaraan : Form
    {
        Karyawan username;
        string id;
        Kendaraan ken;
        BengkelEntities5 upp = new BengkelEntities5();
        public UpKendaraan(string id, Karyawan username)
        {
            this.username = username;
            InitializeComponent();
            this.id = id;
            load();


        }
        public void load()
        {
            var data1 = (from x in upp.Kendaraans where x.Id_Kendaraan == id select x).FirstOrDefault<Kendaraan>();
            txtplat.Text = data1.Kode_Kendaraan;
            txtpemilik.Text = data1.Pemilik;
            textBox5.Text = data1.Alamat;
            textBox4.Text = data1.merk;
            textBox3.Text = data1.Tipe;
            textBox2.Text = data1.Keterangan;
            txtkilometer.Text = data1.Kilometer;




            ken = data1;
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            ken.Kode_Kendaraan = txtplat.Text;
            ken.Pemilik = txtpemilik.Text;
            ken.Alamat = textBox5.Text;
            ken.merk = textBox4.Text;
            ken.Tipe = textBox3.Text;
            ken.Keterangan = textBox2.Text;
            ken.Kilometer = txtkilometer.Text;



            upp.SaveChanges();
            MessageBox.Show("Data Berhasil Diubah...");
            clear();
        }
        public void clear()
        {
            txtplat.Text = "";
            txtpemilik.Text = "";
            textBox5.Text = "";
            textBox4.Text = "";
            textBox3.Text = "";
            textBox2.Text = "";
            txtkilometer.Text = "";

        }
    }
}
