﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Bengkel
{
    public partial class laporanreturbeli : Form
    {
        Karyawan username;
        DateTime mulai, selesai;
        string NAMA;

       

        public laporanreturbeli(Karyawan username, DateTime mulai, DateTime selesai, string NAMA)
        {
            this.username = username;
            this.mulai = mulai;
            this.selesai = selesai;
            this.NAMA = NAMA;
            InitializeComponent();
        }
        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {

            BengkelEntities5 contex = new BengkelEntities5();
            Master_Bengkel data = contex.Master_Bengkel.Where(c => c.Id_Bengkel == username.Bengkel_id).FirstOrDefault();
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            string filename = path + "\\FOTO\\" + data.logo;
            CrystalReport5 laporanbarang = new CrystalReport5();
            laporanbarang.SetParameterValue("@Bengkelid", username.Bengkel_id.ToString());
            laporanbarang.SetParameterValue("@tglmulai", mulai);
            laporanbarang.SetParameterValue("@tglusai", selesai);
            laporanbarang.SetParameterValue("@nama", NAMA);
            laporanbarang.SetParameterValue("@tipe", "1");
            laporanbarang.SetParameterValue("parambengkel", data.Nama);
            laporanbarang.SetParameterValue("paramalamat", data.Alamat);
            laporanbarang.SetParameterValue("paramtelepon", data.Telepon);
            laporanbarang.SetParameterValue("urlupload", filename);
            crystalReportViewer1.ReportSource = laporanbarang;
        }
    }
}
