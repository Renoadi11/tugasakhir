//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bengkel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Jasa_Servis
    {
        public string Id_Jasa { get; set; }
        public string Nama_jasa { get; set; }
        public decimal Harga { get; set; }
        public string Bengek_id { get; set; }
    
        public virtual Master_Bengkel Master_Bengkel { get; set; }
    }
}
