﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class FormtambahKendaraan : Form
    {
        Karyawan user;
        Penjualan_serviss penjualanservis;
        public FormtambahKendaraan(Karyawan user,Penjualan_serviss penjualanservis)
        {
            this.penjualanservis = penjualanservis;
            this.user = user;
            InitializeComponent();
            //defaultform_load();
        }
        public FormtambahKendaraan(Karyawan user)
        {
            this.user = user;
            InitializeComponent();
            //defaultform_load();
        }
        public string get_IDKendaraan()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetidKendaraan id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            kon.Close();
            return "KD" + id;

        }
        public void defaultform_load()
        {
            txtIid.Text = get_IDKendaraan();

        }

        private void btnsimpan_Click(object sender, EventArgs e)
          {
                BengkelEntities5 context = new BengkelEntities5();
                Kendaraan ken = new Kendaraan();
                
                

                ken.Id_Kendaraan = get_IDKendaraan();
                ken.Kode_Kendaraan = txtkode.Text;
                ken.Pemilik = txtnama.Text;
                ken.Alamat = txtalamat.Text;
                ken.merk = txtmerk.Text;
                ken.Tipe = txttipe.Text;
                ken.Keterangan = txtket.Text;
            ken.Kilometer = txtkilometer.Text;
                ken.Bengkel_id = user.Bengkel_id;
                context.Kendaraans.Add(ken);
                context.SaveChanges();
            this.Dispose();
                MessageBox.Show("data Berhasil di input");
            
            clear();
        }
        public void clear()
        {
            txtkode.Text = "";
            txtnama.Text = "";
            txtalamat.Text = "";
            txtmerk.Text = "";
            txttipe.Text = "";
            txtket.Text = "";
            txtkilometer.Text = "";
            penjualanservis.defaultform_load();


        }

        private void txtkilometer_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }
    }
    
}
