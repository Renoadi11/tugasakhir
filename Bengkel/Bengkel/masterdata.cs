﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class masterdata : Form
    {
        Karyawan username;
        utama ut;
        public masterdata(Karyawan username, utama ut)
        {
            this.ut = ut;
            this.username= username;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {
            Kendaraann ken = new Kendaraann(username);
            ken.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Barangg bar = new Barangg(username, ut);
            ut.Hide();
            bar.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            pelnggann pel = new pelnggann();
            pel.Show();
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Suplierr sup = new Suplierr(username,ut);
            sup.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Karyawann kar = new Karyawann(username, ut); 
            kar.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Masterbengkel master = new Masterbengkel(username,ut);
            master.Show();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            Keahliann keh = new Keahliann(username);
            keh.Show();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            Jasaa jas = new Jasaa(username);
            jas.Show();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Penjualan_Barangg pen = new Penjualan_Barangg(username,ut);
            pen.Show();
        }
    }
}
