﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class UcPenjualan : UserControl
    {
        utama ut;
        Karyawan ky;
        public UcPenjualan(utama ut,Karyawan ky)
        {
            this.ut = ut;
            this.ky = ky;
            InitializeComponent();
            load();

        }
        public void load()
        {

            BengkelEntities5 context = new BengkelEntities5();
            var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            if (ben.Booking == 0)
            {
                button1.Hide();
            }
            if(ben.retur_jual == 0)
            {
                button8.Hide();
            }
           
        }

        private void button6_Click(object sender, EventArgs e)
        {
            FormPENJUALAN_BARANGG tampil = new FormPENJUALAN_BARANGG(ky,ut);
            ut.Hide();
            tampil.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Penjualan_serviss tampil = new Penjualan_serviss(ky, ut);
            ut.Hide();
            tampil.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            ReturJuall tampil = new ReturJuall(ky, ut);
            ut.Hide();
            tampil.Show();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Bookingg boking = new Bookingg(ky, ut);
            ut.Hide();
            boking.Show();
        }
    }
}
