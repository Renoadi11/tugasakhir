﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Bengkel
{
    public partial class laporanbarang : Form
    {
        Karyawan username;
        public laporanbarang(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
        }

        private void crystalReportViewer1_Load(object sender, EventArgs e)
        {
            BengkelEntities5 contex = new BengkelEntities5();
            Master_Bengkel data = contex.Master_Bengkel.Where(c => c.Id_Bengkel == username.Bengkel_id).FirstOrDefault();
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            string filename = path + "\\FOTO\\" + data.logo;
            CrystalReport10 laporanbarang = new CrystalReport10();
            laporanbarang.SetParameterValue("@bengkelid", username.Bengkel_id.ToString());
           
           // laporanbarang.SetParameterValue("@tipe", "1");
            laporanbarang.SetParameterValue("parambengkel", data.Nama);
            laporanbarang.SetParameterValue("paramalamat", data.Alamat);
            laporanbarang.SetParameterValue("paramtelepon", data.Telepon);
            laporanbarang.SetParameterValue("urlupload", filename);
            crystalReportViewer1.ReportSource = laporanbarang;
        }
    }
}
