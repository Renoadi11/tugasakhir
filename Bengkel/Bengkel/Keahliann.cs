﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Bengkel.Model;


namespace Bengkel
{
    public partial class Keahliann : Form
    {
        Karyawan username;
        public Keahliann(Karyawan username)
        {
            this.username = username;
            InitializeComponent();
            load();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TambahKeahliann tam = new TambahKeahliann(username);
            tam.Show();
        }
        private void load()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Keahlian, Nama_Keahlian from Keahlian where Bengkel_id =@id;";
            List<Modelkeahlian> data = kon.Query<Modelkeahlian>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();

            dataGridView1.DataSource = data;
        }

        private void button4_Click(object sender, EventArgs e)
        {

            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            upkeahliann keh = new upkeahliann(up, username);
            keh.Show();
        }
    }
}
