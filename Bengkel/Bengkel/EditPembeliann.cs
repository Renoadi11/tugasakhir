﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms;
using Bengkel.Model;
using System.Globalization;
using System.IO;

namespace Bengkel
{
    public partial class Edit_Pembeliann : Form
    {
        Karyawan username;
        private modelbarangnotajual barnotj { get; set; }
        Nota_Pembelian_Barang not;
        BengkelEntities5 upp = new BengkelEntities5();
        PembelianBarangg pb;
        public Edit_Pembeliann(modelbarangnotajual barnotj, PembelianBarangg pb)
        {
            this.pb = pb;
            this.barnotj = barnotj;
            Nota_Pembelian_Barang not;
            InitializeComponent();
            defaultform_load();
        }
        public void defaultform_load()
        {
            txtbarangg.Text = barnotj.NamaBarang;
            txtjumlah.Text = barnotj.Jumlah.ToString();
            txtpotongan.Text = barnotj.Potongan.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Edit_Pembeliann ed = this;

            foreach (modelbarangnotajual item in pb.datanotaa.Where(u => u.IdBarang == barnotj.IdBarang))
            {
                item.Jumlah = int.Parse(txtjumlah.Text);
                item.Potongan = int.Parse(txtpotongan.Text);

            }
            pb.resetdatagridview();
            pb.Focus();
            ed.Close();

        }
    }
}
