﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using System.Data.SqlClient;

namespace Bengkel
    
{
    public partial class TambahMekanikk : Form
    {
        Mekanikk meka;
        Karyawan username;
        public TambahMekanikk(Karyawan username, Mekanikk meka)
        {
            this.meka = meka;
            this.username = username;
            InitializeComponent();
            defaultform_load();
        }
        public string get_idmekanik()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIdMekanik id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "MK" + id;

        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            try
            {
                BengkelEntities5 context = new BengkelEntities5();
                Mekanik mek = new Mekanik();

                mek.Id_Mekanik = get_idmekanik();
                //  mek.Nama = txtnama.Text;
                if (txtnama.Text == "")
                {
                    MessageBox.Show("ISI NAMA DULU");
                    txtnama.Focus();
                }

                //mek.Alamat = txtalamat.Text;
                if(txtalamat.Text=="")
                {
                    MessageBox.Show("ISI ALAMAT DULU");
                    txtalamat.Focus();
                }

               // mek.telepon = txttelpon.Text;
               if(txttelpon.Text=="")
                {
                    MessageBox.Show("ISI TELEPON DULU");
                    txttelpon.Focus();
                }
               else
                {
                    mek.Nama = txtnama.Text;
                    mek.Alamat = txtalamat.Text;
                    mek.telepon = txttelpon.Text;
                    mek.bengkel_id = username.Bengkel_id;

                    context.Mekaniks.Add(mek);
                    context.SaveChanges();
                    MessageBox.Show("data Berhasil di input");
                    Clear();
                }

               

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Clear()
        {
            txtnama.Text = "";
            txtalamat.Text = "";
            txttelpon.Text = "";
        }
        public void defaultform_load()
        {
            txtikaryawan.Text = get_idmekanik();

        }

        private void TambahMekanikk_FormClosing(object sender, FormClosingEventArgs e)
        {
            meka.load();
        }
    }
}
