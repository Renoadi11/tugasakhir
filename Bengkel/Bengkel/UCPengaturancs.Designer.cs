﻿namespace Bengkel
{
    partial class UCPengaturancs
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtgaji = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.ckbooking = new System.Windows.Forms.CheckBox();
            this.ckreturpen = new System.Windows.Forms.CheckBox();
            this.ckdiskon = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.CadetBlue;
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtgaji);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.checkBox4);
            this.groupBox1.Controls.Add(this.ckbooking);
            this.groupBox1.Controls.Add(this.ckreturpen);
            this.groupBox1.Controls.Add(this.ckdiskon);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(310, 256);
            this.groupBox1.TabIndex = 23;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PENGATURAN";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(226, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "%";
            // 
            // txtgaji
            // 
            this.txtgaji.Location = new System.Drawing.Point(174, 96);
            this.txtgaji.Name = "txtgaji";
            this.txtgaji.Size = new System.Drawing.Size(46, 22);
            this.txtgaji.TabIndex = 12;
            this.txtgaji.TextChanged += new System.EventHandler(this.txtgaji_TextChanged);
            this.txtgaji.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.txtgaji_ControlAdded);
            this.txtgaji.Enter += new System.EventHandler(this.txtgaji_Enter);
            this.txtgaji.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgaji_KeyPress);
            this.txtgaji.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtgaji_KeyUp);
            this.txtgaji.Leave += new System.EventHandler(this.txtgaji_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(106, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "KOMISI :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-5, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(922, 25);
            this.label3.TabIndex = 10;
            this.label3.Text = "______________________________________________________________________";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(0, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "PEMBELIAN";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "PENJUALAN";
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(4, 176);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(169, 20);
            this.checkBox4.TabIndex = 5;
            this.checkBox4.Text = "RETUR PEMBELIAN";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // ckbooking
            // 
            this.ckbooking.AutoSize = true;
            this.ckbooking.Location = new System.Drawing.Point(6, 91);
            this.ckbooking.Name = "ckbooking";
            this.ckbooking.Size = new System.Drawing.Size(94, 20);
            this.ckbooking.TabIndex = 4;
            this.ckbooking.Text = "BOOKING";
            this.ckbooking.UseVisualStyleBackColor = true;
            this.ckbooking.CheckedChanged += new System.EventHandler(this.ckbooking_CheckedChanged);
            // 
            // ckreturpen
            // 
            this.ckreturpen.AutoSize = true;
            this.ckreturpen.Location = new System.Drawing.Point(121, 65);
            this.ckreturpen.Name = "ckreturpen";
            this.ckreturpen.Size = new System.Drawing.Size(173, 20);
            this.ckreturpen.TabIndex = 3;
            this.ckreturpen.Text = "RETUR PENJUALAN";
            this.ckreturpen.UseVisualStyleBackColor = true;
            this.ckreturpen.CheckedChanged += new System.EventHandler(this.ckreturpen_CheckedChanged);
            // 
            // ckdiskon
            // 
            this.ckdiskon.AutoSize = true;
            this.ckdiskon.Location = new System.Drawing.Point(6, 65);
            this.ckdiskon.Name = "ckdiskon";
            this.ckdiskon.Size = new System.Drawing.Size(83, 20);
            this.ckdiskon.TabIndex = 2;
            this.ckdiskon.Text = "DISKON";
            this.ckdiskon.UseVisualStyleBackColor = true;
            this.ckdiskon.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // UCPengaturancs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UCPengaturancs";
            this.Size = new System.Drawing.Size(316, 257);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox ckbooking;
        private System.Windows.Forms.CheckBox ckreturpen;
        private System.Windows.Forms.CheckBox ckdiskon;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtgaji;
        private System.Windows.Forms.Label label4;
    }
}
