﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;

namespace Bengkel
{
    public partial class DaftarBookingg : Form
    {
        Karyawan username;
        Penjualan_serviss ps;
        public DaftarBookingg(Karyawan username, Penjualan_serviss ps)
        {

            this.ps = ps;
            this.username = username;
            InitializeComponent();
            defaultform_load();

        }

        private void DaftarBookingg_Load(object sender, EventArgs e)
        {

            

        }
        public void defaultform_load()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select * from Booking where Status_id= 1 and convert(date, Tanggal_booking) = convert (date, getdate()) and bengekl_id=@id;";
            List<Bookinggg> data = kon.Query<Bookinggg>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = data;

            DataGridViewLinkColumn Editlink = new DataGridViewLinkColumn();
            Editlink.UseColumnTextForLinkValue = true;
            Editlink.Text = "Proses";
            dataGridView1.Columns.Add(Editlink);

            DataGridViewLinkColumn hapuslink= new DataGridViewLinkColumn();
            hapuslink.UseColumnTextForLinkValue = true;
            hapuslink.Text = "Batal";
            dataGridView1.Columns.Add(hapuslink);

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            kon.Close();

            if (e.ColumnIndex == 0)
            {

                string data = dataGridView1[2, e.RowIndex].Value.ToString();
               // MessageBox.Show(data);
                kon.Open();
                string sql = "select db.id_barang as Id_Barang, db.jumlah as Jumlah, db.nama_barang as Nama_Barang, db.potongan as Potongan, db.harga as Harga from Detail_booking as db inner join barang as b on b.Id_Barang = db.id_barang where b.Tipe_barang =@tp and db.Booiking_id=@idboking;";
                string sql1 = "select db.id_barang  as Id_Servis, db.jumlah as Jumlah, db.nama_barang as Nama_Servis, db.potongan as Potongan, db.harga as Harga from Detail_booking as db inner join barang as b on b.Id_Barang = db.id_barang where b.Tipe_barang =@tp and db.Booiking_id=@idboking;";

                List<ModelBarangNota> data1 = kon.Query<ModelBarangNota>(sql, new { idboking = data, tp = "1" }).ToList();
                List< Modelbarangservis > data2 = kon.Query<Modelbarangservis>(sql1, new { idboking = data, tp = "2" }).ToList();
                //List<modelserviss> data3 = konn.Query<modelserviss>(sql3, new { id = username.Bengkel_id, tp = "2" }).ToList();
                ps.datanota2 = data1;
                ps.datanota1 = data2;
                ps.btndiskon.Text="test";
                ps.resetdatagridview();
                ps.Refresh();

               // ps.Show();
              // this.Close();

            }
        }
    }
}
