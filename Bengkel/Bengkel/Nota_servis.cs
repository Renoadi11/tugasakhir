//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Bengkel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Nota_servis
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Nota_servis()
        {
            this.detail_nota_servis = new HashSet<detail_nota_servis>();
        }
    
        public string Id_Nota_servis { get; set; }
        public System.DateTime Tanggal_servis { get; set; }
        public string Nomor_Plat { get; set; }
        public Nullable<int> Kilometer { get; set; }
        public string Keterangan { get; set; }
        public decimal total { get; set; }
        public string Pelanggan_Id { get; set; }
        public string Karyawan_Id { get; set; }
        public int status { get; set; }
        public string Bengkel_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<detail_nota_servis> detail_nota_servis { get; set; }
        public virtual Karyawan Karyawan { get; set; }
        public virtual Master_Bengkel Master_Bengkel { get; set; }
        public virtual Master_Status Master_Status { get; set; }
        public virtual Pelanggan Pelanggan { get; set; }
    }
}
