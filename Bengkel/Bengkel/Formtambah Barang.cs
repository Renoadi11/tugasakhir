﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using Bengkel.Model;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class Tambah_Barang : Form
    {
        Karyawan username;
        Barangg b;
        public Tambah_Barang(Karyawan username,Barangg b)
        {
            this.b = b;
            this.username = username;
            InitializeComponent();
            defaultform_load();
        }
        public string get_IdBarang()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIdBarang id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "BR" + id;
        }
    
        public void defaultform_load()
        {
            txtkode.Text = get_IdBarang();

            string bdd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection konn = new SqlConnection(bdd);
            konn.Open();
            string sql1 = "select Id_Tipe, Nama from Tipe;";

            List<ModelTipe> data3 = konn.Query<ModelTipe>(sql1, new { id = username.Bengkel_id }).ToList();
            konn.Close();

            foreach (var item in data3)
            {
                cbotipe.Items.Add(item);
            }

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        public void resetdata()
        {
            
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            Barang bar = new Barang();
            

            var tipe = (ModelTipe)cbotipe.SelectedItem;
            bar.Id_Barang= get_IdBarang();
           // bar.Nama_Barang = txtbarang.Text;
           if(txtbarang.Text=="")
            {
                MessageBox.Show("ISI NAMA BARANG DULU");
                txtbarang.Focus();
            }
           // bar.Jenis_Barang = cbojenis.SelectedItem.ToString();
           if(cbojenis.SelectedIndex== -1)
            {
                MessageBox.Show("ISI Jenis DULU");

            }
          
            if (rdoaktif.Checked)
            {
                bar.Hapus = 0;
            }
            else
            {

                // sup = null;
                MessageBox.Show("PILIH AKTIF");

            }

            // bar.Stok_Dimiliki =Convert.ToInt32(txtjumlahstok.Text.ToString());
            if (txtjumlahstok.Text=="")
            {
                MessageBox.Show("ISI DULU JUMLAH");
                txtjumlahstok.Focus();
            }
            //bar.Harga_Satuan = Convert.ToDecimal(txthargajual.Text.ToString());
            if(txthargajual.Text=="")
            {
                MessageBox.Show("ISI DULU HARGA");
                txthargajual.Focus();
            }
          
            // bar.Stok_minimal =Convert.ToInt32(textBox1.Text);
            if (textBox1.Text == "")
            {
                MessageBox.Show("ISI DULU STOK");
                textBox1.Focus();
            }
          //  bar.Harga_Beli = Convert.ToDecimal(txthargapokok.Text.ToString());
          if(txthargapokok.Text=="")
            {
                MessageBox.Show("ISI DULU HARGA POKOK");
                txthargapokok.Focus();
            }
            //  bar.penambahan_Stok = Convert.ToInt32(txtpenambahanstok.Text.ToString());

           
                else
            {
                bar.Nama_Barang = txtbarang.Text;
                bar.Jenis_Barang = cbojenis.SelectedItem.ToString();
                bar.Stok_Dimiliki = Convert.ToInt32(txtjumlahstok.Text.ToString());
                bar.Harga_Satuan = Convert.ToDecimal(txthargajual.Text.ToString());
                bar.No_rak_barang = txtrak.Text;
                bar.Stok_minimal = Convert.ToInt32(textBox1.Text);
                bar.Harga_Beli = Convert.ToDecimal(txthargapokok.Text.ToString());
                bar.penambahan_Stok = Convert.ToInt32(txtpenambahanstok.Text.ToString());
                bar.Bengkel_id = username.Bengkel_id;
                bar.Tipe_barang = tipe.Id_Tipe;
                bar.No_rak_barang = txtrak.Text;

                bar.Keterangan = txtketerangan.Text;

                context.Barangs.Add(bar);
                context.SaveChanges();
                MessageBox.Show("data Berhasil di input");
                clear();
              
                
                

            }

        }
        public void clear()
        {
            txtbarang.Text = "";
            txthargajual.Text = "";
            txthargapokok.Text = "";
            txtjumlahstok.Text = "";
            txtketerangan.Text = "";
            txtkode.Text = "";
            txtpenambahanstok.Text = "";
            cbojenis.Text = "";
            txtrak.Text = "";
            textBox1.Text = "";
            cbotipe.Text = "";
        }

        private void txtjumlahstok_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void txthargajual_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void txtpenambahanstok_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void txthargapokok_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void cbotipe_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbotipe.SelectedIndex==1)
            {
                label4.Visible = false;
                txtjumlahstok.Visible = false;
                label13.Visible = false;
                textBox1.Visible = false;
                label18.Visible = false;
                label19.Visible = false;
                label7.Visible = false;
                txtrak.Visible = false;
            }
            else
            {
                label4.Visible = true;
                txtjumlahstok.Visible = true;
                label13.Visible = true;
                textBox1.Visible = true;
                label18.Visible = true;
                label19.Visible = true;
                label7.Visible = true;
                txtrak.Visible = true;
            }
           


        }

        private void Tambah_Barang_FormClosing(object sender, FormClosingEventArgs e)
        {
            b.load();
        }
    }
    
    
   
}
