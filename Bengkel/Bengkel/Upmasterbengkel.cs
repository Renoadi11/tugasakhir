﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;


namespace Bengkel
{
    public partial class Upmasterbengkel : Form
    {
        Karyawan username;
        string id;
        Master_Bengkel ms;
        BengkelEntities5 upp = new BengkelEntities5();
        
        public Upmasterbengkel(string id, Karyawan username)
        {
            InitializeComponent();
            this.username = username;
            this.id = id;
            load();
        }

        public void load()
        {
            
             var data1 = (from x in upp.Master_Bengkel where x.Id_Bengkel == id select x).FirstOrDefault<Master_Bengkel>();
            
            //txtikaryawan.Text = data1.Id_Bengkel;
            txtnama.Text = data1.Nama;
            txtalamat.Text = data1.Alamat;
            txttelepon.Text = data1.Alamat;
            lblfilename.Text = data1.logo;

            ms = data1;
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
           
            ms.Nama = txtnama.Text;
            ms.Alamat = txtalamat.Text;
            ms.Telepon = txttelepon.Text;
            ms.logo = lblfilename.Text;

            upp.SaveChanges();
            MessageBox.Show("Data Berhasil Diubah...");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog upload = new OpenFileDialog();
            upload.ShowDialog();
            upload.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            // lblfilename.Text = upload.FileName;
            lblfilename.Visible = true;
            //   MessageBox.Show(upload.SafeFileName);
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            File.Copy(upload.FileName, path + "\\FOTO\\" + upload.SafeFileName);
            lblfilename.Text = upload.SafeFileName;
        }

        private void txttelepon_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }
    }
    
}
