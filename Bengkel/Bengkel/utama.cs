﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;
namespace Bengkel
{
    public partial class utama : Form
    {
        Karyawan user;
        
       
        public utama( Karyawan user)
        {
            this.user = user;
            InitializeComponent();
            this.Closing += new System.ComponentModel.CancelEventHandler(FormClosingEventCancle_Closing);
            lbl1.Text = user.Nama;
            lbl1.Visible = true;
             defaultform_load();
            loadedit();
            
        }
        private void FormClosingEventCancle_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            DialogResult dr = MessageBox.Show("Apakah yakin ingin keluar dari aplikasi ini?", "Konfirmasi", MessageBoxButtons.YesNo);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;

            }


            else
            {
                e.Cancel = false;
                Login log = new Login();
                this.Hide();
                log.Show();
                //Application.Exit();
            }

               
          

        }
        private void button1_Click(object sender, EventArgs e)
        {

            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcMasterdata(this, user));
            //masterdata tampil = new masterdata(user);
            //tampil.Show();


        }
        public void defaultform_load()
        {
            timer1.Interval = 1000;
            timer1.Enabled = true;
            timer2.Enabled = true;

            dataGridView1.DataSource = null;
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Nama_Barang,Stok_Dimiliki, Stok_minimal from Barang where Stok_Dimiliki <= Stok_minimal and Bengkel_id =@id and Tipe_barang =1 and Hapus=0";
            //string sql2 = "select *from Nota_jual_barang as b inner join Status_servis as sv on sv.id_status_penjualan =b.status_id where b.status_id != 3";
            List<ModelBarangg> data = kon.Query<ModelBarangg>(sql, new { id = user.Bengkel_id }).ToList();
           // List<modelinfo> data2 = kon.Query<modelinfo>(sql2).ToList();
           
            //dataGridView2.DataSource = data2;

            kon.Close();
            dataGridView1.DataSource = data;
            if (user.Status_pengguna==0)
            {
                ps.HideDropDown();
               ps.Visible = false;
            }
            if(user.Status_pengguna==0)
            {
                plapo.HideDropDown();
                plapo.Visible = false;
            }
           
            
        }
        public void loadedit()
        {
            DataGridViewLinkColumn Editlink = new DataGridViewLinkColumn();
            Editlink.UseColumnTextForLinkValue = true;
            Editlink.Text = "edit";
            //dataGridView2.Columns.Add(Editlink);
        }
        
        
        

        private void txtutama_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcPembelian(this, user));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Penjualan_Barangg tampil = new Penjualan_Barangg(user);
            //tampil.Show();
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcPenjualan(this,user));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcLaporan(this, user));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UCPengaturancs(this, user));
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbl1_Click(object sender, EventArgs e)
        {

        }

        private void utama_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnkluar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Benarkah Anda Ingin Keluar  dari aplikasi ini ???", "Konfirmasi",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Login log = new Login();
                this.Hide();
                log.Show();

                //Application.Exit();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbljam.Text = DateTime.Now.ToLongTimeString();
            lbltgl.Text = DateTime.Now.ToLongDateString();
        }
       
        private void timer2_Tick(object sender, EventArgs e)
        {
            if(label2.Left<820)
            {
                label2.Left = label2.Left + 4;
            }
            else
            {
                label2.Left = label2.Left - 1000;
            }
        }

        private void mASTERDATAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcMasterdata(this, user));

        }

        private void pEMBELIANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcPembelian(this, user));
        }

        private void pENJUALANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcPenjualan(this, user));
        }

        private void lAPORANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UcLaporan(this, user));
        }

        private void pENGATURANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pnUtama.Controls.Clear();
            pnUtama.Controls.Add(new UCPengaturancs(this, user));
        }

        private void bARANGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Barangg tampil = new Barangg(user,this);
            this.Hide();
            tampil.Show();
        }

        private void pEMBELIANBARANGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PembelianBarangg tampil = new PembelianBarangg(user,this);
            this.Hide();
            tampil.Show();
        }

        private void sUPPLIERToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Suplierr tampil = new Suplierr(user,this);
            this.Hide();
            tampil.Show();
        }

        private void pENGGUNAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Karyawann tampil = new Karyawann(user,this);
            this.Hide();
            tampil.Show();
        }

        private void mASTERBENGKELToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Masterbengkel tam = new Masterbengkel(user,this);
            this.Hide();
            tam.Show();
        }

        private void mEKANIKToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Mekanikk mek = new Mekanikk(user,this);
            this.Hide();
            mek.Show();
        }

        private void rETURPEMBELIANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Retur_Belii ret = new Retur_Belii(user,this);
            this.Hide();
            ret.Show();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Penjualan_serviss tampil = new Penjualan_serviss(user,this);
            this.Hide();
            tampil.Show();
        }

        private void bOOKINGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bookingg boking = new Bookingg(user,this);
            this.Hide();
            boking.Show();
        }

        private void rETURPENJUALANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReturJuall tampil = new ReturJuall(user,this);
            this.Hide();
            tampil.Show();
        }

        private void lAPORANPENJUALANBARANGDANSERVISToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaporanPenjualan tampil = new LaporanPenjualan(user,this);
            this.Hide();
            tampil.Show();
        }

        private void lAPORANSTOKBARANGToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void lAPORANPEMBELIANBARANGToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formlaporanpembelian lapo = new Formlaporanpembelian(user,this);
            lapo.Hide();
            lapo.Show();
        }

        private void lAPORANRETURPENJUALANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formlaporanretujual lapo = new Formlaporanretujual(user);
            lapo.Hide();
            lapo.Show();
        }

        private void lAPORANRETURPEMBELIANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formlaporanreturbeli lapo = new Formlaporanreturbeli(user,this);
            lapo.Hide();
            lapo.Show();
        }

        private void pnUtama_Paint(object sender, PaintEventArgs e)
        {

        }

        private void utama_FormClosing(object sender, FormClosingEventArgs e)
        {

        }


        //private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        //{
        //    string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
        //    SqlConnection kon = new SqlConnection(bd);
        //    kon.Open();
        //    string sql = "select N.* from Detail_Jual_Barang as N inner join barang as b on b.Id_Barang = N.id_barang where N.id_nota_jual =@nota and b.Tipe_barang = @tipe" ;
        //    string sql2 = "select N.* from Nota_jual_barang as n where N.id_nota_jual=@not"; 

        //    if (e.ColumnIndex == 0)
        //    {

        //    //   string data= dataGridView2[1, e.RowIndex].Value.ToString();
        //        MessageBox.Show(data);

        //        Penjualan_serviss pen = new Penjualan_serviss(user, this);
        //        Nota_jual_barang nota = kon.Query<Nota_jual_barang>(sql2, new { not = data }).FirstOrDefault();
        //        pen.datanota1 = kon.Query<Modelbarangservis>(sql, new { Nota = data, tipe= 2 }).ToList();
        //        pen.datanota2 = kon.Query<ModelBarangNota>(sql, new { Nota = data, tipe = 1 }).ToList();

        //        pen.lblnota.Text = nota.Id_nota_Jual;
        //        pen.lbltgl.Text = nota.Tanggal_Jual.ToString();

        //        if(nota.status_id == 1)
        //        {
        //            pen.cbostatus.SelectedIndex = 0;
        //        }
        //        else if(nota.status_id == 2)
        //        {
        //            pen.cbostatus.SelectedIndex = 1;
        //        }
        //        else if (nota.status_id == 3)
        //        {
        //            pen.cbostatus.SelectedIndex = 2;
        //        }
        //        pen.button9.Enabled = false;



        //        //pen.datanota1 = kon.Query<ModelBarangNota>(sql2, new { Nota = data, tipe = 2 }).ToList();
        //        //pen.datanota2 = kon.Query<ModelBarangNota>(sql2, new { Nota = data, tipe = 1 }).ToList();

        //        pen.resetdatagridview();
        //        pen.Show();

        //    }
        //}
    }
}
