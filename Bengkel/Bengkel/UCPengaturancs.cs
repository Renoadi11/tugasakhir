﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class UCPengaturancs : UserControl
    {
        utama ut;
        Karyawan ky;
        public UCPengaturancs(utama ut, Karyawan ky)
        {
            this.ut = ut;
            this.ky = ky;
            InitializeComponent();
            load();
        }
        public void load()
        {
            BengkelEntities5 context = new BengkelEntities5();
            var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            if (ben.Booking == 1) { ckbooking.Checked = true; } else { ckbooking.Checked = false; }
            if (ben.Diskon == 1) { ckdiskon.Checked = true; } else { ckdiskon.Checked = false; }
            if (ben.retur_jual == 1) { ckreturpen.Checked = true; } else { ckreturpen.Checked = false; }
            if (ben.Retur == 1) { checkBox4.Checked = true; } else { checkBox4.Checked = false; }
            txtgaji.Text = ben.fee.ToString();


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            validasicek();
        }

        private void ckbooking_CheckedChanged(object sender, EventArgs e)
        {
            validasicek();
        }
        public void validasicek()
        {
            BengkelEntities5 context = new BengkelEntities5();
            var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            if (ckbooking.Checked == true) { ben.Booking = 1; } else { ben.Booking = 0; } 
            if (ckdiskon.Checked == true) { ben.Diskon = 1; } else { ben.Diskon = 0; }
            if (ckreturpen.Checked == true) { ben.retur_jual = 1; } else { ben.retur_jual = 0; }
            if (checkBox4.Checked == true) { ben.Retur = 1; } else { ben.Retur = 0; }
          
            context.SaveChanges();
        }

        private void ckreturpen_CheckedChanged(object sender, EventArgs e)
        {
            validasicek();
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            validasicek();
        }

        private void txtgaji_TextChanged(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
           // txtgaji.Text = ben.fee.ToString();
           if(txtgaji.Text.Count()>0)
            {
                ben.fee = int.Parse(txtgaji.Text);
                context.SaveChanges();
                load();
            }
           
            
        }

        private void txtgaji_Leave(object sender, EventArgs e)
        {

            //BengkelEntities5 context = new BengkelEntities5();
            //var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            //txtgaji.Text = ben.fee.ToString();
            //ben.fee = int.Parse(txtgaji.Text);
            //load();
        }

        private void txtgaji_ControlAdded(object sender, ControlEventArgs e)
        {
            //BengkelEntities5 context = new BengkelEntities5();
            //var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            //txtgaji.Text = ben.fee.ToString();
            //ben.fee = int.Parse(txtgaji.Text);
            //load();
        }

        private void txtgaji_Enter(object sender, EventArgs e)
        {

        }

        private void txtgaji_KeyUp(object sender, KeyEventArgs e)
        {
            //BengkelEntities5 context = new BengkelEntities5();
            //var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            //txtgaji.Text = ben.fee.ToString();
            //ben.fee = int.Parse(txtgaji.Text);
            //load();
        }

        private void txtgaji_KeyPress(object sender, KeyPressEventArgs e)
        {
            //BengkelEntities5 context = new BengkelEntities5();
            //var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == ky.Bengkel_id select bk).FirstOrDefault();
            //txtgaji.Text = ben.fee.ToString();
            //ben.fee = int.Parse(txtgaji.Text);
            //load();
        }
    }
}
