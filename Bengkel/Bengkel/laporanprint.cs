﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Bengkel
{
    public partial class laporanprint : Form
    {
        Karyawan username;
        DateTime mulai, selesai;

       

        public laporanprint(Karyawan username, DateTime mulai, DateTime selesai)
        {
            this.username = username;
            this.mulai = mulai;
            this.selesai = selesai;
            InitializeComponent();
        }
        private void laporanprint_Load(object sender, EventArgs e)
        {
            BengkelEntities5 contex = new BengkelEntities5();
            Master_Bengkel data = contex.Master_Bengkel.Where(c => c.Id_Bengkel == username.Bengkel_id).FirstOrDefault();
            var path = Path.GetDirectoryName(Application.ExecutablePath);
            string filename = path + "\\FOTO\\" + data.logo;

            //string a = "C:Users\\Reno Adi\\Documents\\Visual Studio 2015\\Projects\\tugasakhir\\Bengkel\\Bengkel\\bin\\Debug\\IMG_20171130_152423.jpg";
            CrystalReport1 laporanbarang = new CrystalReport1();
            laporanbarang.SetParameterValue("@bengkelid", username.Bengkel_id.ToString());
            laporanbarang.SetParameterValue("@tanggalmulai", mulai);
            laporanbarang.SetParameterValue("@tanggalselsai", selesai);
            laporanbarang.SetParameterValue("@tipe", "1");
            laporanbarang.SetParameterValue("paramnamabengkel", data.Nama);
            laporanbarang.SetParameterValue("paramalamat", data.Alamat);
            laporanbarang.SetParameterValue("paramtelepon",data.Telepon);
            laporanbarang.SetParameterValue("urllogo", filename);
            crystalReportViewer1.ReportSource = laporanbarang;
        }
    }
}
