﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    class modelmekanik
    {
        public string Id_Mekanik { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string telepon { get; set; }

        public override string ToString()
        {
            return Nama;
        }

    }
}
