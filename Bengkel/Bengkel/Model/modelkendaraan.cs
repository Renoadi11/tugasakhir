﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    class modelkendaraan
    {
        public string Id_Kendaraan { get; set; }
        public string Pemilik { get; set; }
        public string Alamat { get; set; }
        public string merk { get; set; }
        public string Tipe { get; set; }
        //public string Keterangan { get; set; }
        public string kode_Kendaraan { get; set; }
        public string Kilometer { get; set; }
        //public string Bengkel_id { get; set; }
        public override string ToString()
        {
            return kode_Kendaraan;
        }

    }
}
