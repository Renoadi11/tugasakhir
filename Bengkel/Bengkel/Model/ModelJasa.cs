﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Bengkel.Model
{
    class ModelJasa
    {
        public string Id_Jasa { get; set; }
        public string Nama_jasa { get; set; }
        public decimal Harga { get; set; }
        //  public string Bengek_id { get; set; }
        public override string ToString()
        {
            return Nama_jasa + " (" + Harga.ToString("C", new CultureInfo("id-ID")) + ")";
        }
    }
}
