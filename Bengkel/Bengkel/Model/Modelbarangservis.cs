﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    public class Modelbarangservis
    {
        public string IdServis { get; set; }
        public string NamaServis{ get; set; }
      //  public int Jumlah { get; set; }
        public int Potongan { get; set; }
        public int Harga { get; set; }
        public int SubTotal { get { return ( Harga) - Potongan; } set { } }

    }
}
