﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    class Modelbengkel
    {
        public string Id_Bengkel { get; set; }
        public string NAMA { get; set; }
        public string ALAMAT { get; set; }
        public string TELEPON { get; set; }
        public string LOGO { get; set; }

        public override string ToString()
        {
            return NAMA;
        }
    }
}
