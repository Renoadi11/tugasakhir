﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    public class Modelbarangretur
    {
        public string Id_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public int Jumlah { get; set; }
        // public int Potongan { get; set; }
        public int Harga { get; set; }
        public int Sub_Total
        {
            get { return (Jumlah * Harga); }
            set { }
        }
    }

        
}
