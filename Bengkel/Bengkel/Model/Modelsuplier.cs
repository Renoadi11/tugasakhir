﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    class Modelsuplier
    {
        public string Id_Suplier { get; set; }
        public string Nama_Suplier { get; set; }
        public string Alamat { get; set; }
        public string Telpon { get; set; }
        //  public string Bengkel_id { get; set; }

        public override string ToString()
        {
            return Nama_Suplier;
        }

    }
}
