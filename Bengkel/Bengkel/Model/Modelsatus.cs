﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    class Modelsatus
    {
        public int id_status_penjualan { get; set; }
        public string nama { get; set; }
        public override string ToString()
        {
            return nama;
        }
    }
}
