﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Bengkel.Model
{
    class ModelBarang
    {
        public string Id_Barang { get; set; }
        public string Nama_Barang { get; set; }
        public int Harga_Satuan { get; set; }
        public decimal Harga_Beli { get; set; }
        public int Stok_Dimiliki { get; set; }
       // public int Stok_minimal { get; set; }
       // public string Status_barang { get; set; }
       // public string Jenis_Barang { get; set; }
      //  public string No_rak_barang { get; set; }
        public string Keterangan { get; set; }
        


        // public string Karyawan_Id { get; set; }
        // public string Nama_Suplier { get; set; }
       // public Nullable<int> penambahan_Stok { get; set; }
        //  public string Bengkel_id { get; set; }
        public override string ToString()
        {
            return Nama_Barang + " (" + Harga_Satuan.ToString("C", new CultureInfo("id-ID")) + ")";
        }
    }
    
}
