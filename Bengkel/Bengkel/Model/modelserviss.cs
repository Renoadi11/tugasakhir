﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;


namespace Bengkel.Model
{
    class modelserviss
    {
        public string IdServis { get; set; }
        public string NamaServis { get; set; }
        public int Harga_Satuan { get; set; }
        public decimal Harga_Beli { get; set; }
        public int Stok_Dimiliki { get; set; }
        public int Stok_minimal { get; set; }
        public string Status_barang { get; set; }
        public string Jenis_Barang { get; set; }
        public string No_rak_barang { get; set; }
        public string Keterangan { get; set; }

        public override string ToString()
        {
            return NamaServis + " (" + Harga_Satuan.ToString("C", new CultureInfo("id-ID")) + ")";
        }
    }
}
