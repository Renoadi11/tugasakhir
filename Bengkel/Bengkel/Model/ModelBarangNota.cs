﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bengkel.Model
{
    public class ModelBarangNota
    {
        public string IdBarang{ get; set; }
        public string NamaBarang { get; set; }
        public int Jumlah { get; set; }
        public int Potongan{ get; set; }
        public int Harga { get; set; }
        public int SubTotal { get { return (Jumlah * Harga) - Potongan; } set { } }

    }
}
