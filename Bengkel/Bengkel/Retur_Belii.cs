﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;
namespace Bengkel
{
    public partial class Retur_Belii : Form
    {
        Karyawan username;
        utama ut;
        cbvalue cbo;
        public List<Modelbarangretur> datanota = new List<Modelbarangretur>();
        public Retur_Belii(Karyawan username, utama ut)
        {
            this.username = username;
            this.ut = ut;
            InitializeComponent();
            defaultform_load();
            lbladmin.Text = username.Nama;
        }
        public string get_idreturjual()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select CONCAT(YEAR( GETDATE()),RIGHT('00' + CONVERT(VARCHAR(12),  MONTH(GETDATE())), 2),RIGHT('0000' + CONVERT(VARCHAR(12), next value for GetIdReturbeli ), 4)) id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "RTUB" + id;
        }
        public void defaultform_load()
        {
            lblretur.Text = get_idreturjual();
            lbltgl.Text = DateTime.Now.ToString();

            string bdd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection konn = new SqlConnection(bdd);

            string sql = "select n.Id_nota_pembelian,s.Nama_Suplier from Nota_Pembelian_Barang N inner join Suplier s on n.Suplier_Id = s.Id_Suplier "+
                         "where n.Bengkel_id = @id;";
            string sql2 = " select Id_Suplier, Nama_Suplier, Alamat, Telpon from Suplier where Bengkel_id = @id and status = 0 ;";

            List<cbvalue> data = konn.Query<cbvalue>(sql, new { id = username.Bengkel_id }).ToList();
            //List<Modelsuplier> data2 = konn.Query<Modelsuplier>(sql2, new { id = username.Bengkel_id }).ToList();
            konn.Close();
            foreach (var item in data)
            {
                cbonota.Items.Add(item);
            }
        }
        public void resetdatagridview()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = datanota;
            // txtttotal.Text = datanota.Sum(i => i.Sub_Total).ToString();
            txttotal.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));
            // txtsisa.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));
        }

        private void btntambah_Click(object sender, EventArgs e)
        {
            int jumlah = int.Parse(txtjumlah.Text);
            modelnotajuallll barang = (modelnotajuallll)cbobarang.SelectedItem;
            if(cbobarang.SelectedIndex==-1)
            {
                MessageBox.Show("Isi Barang Terlebih Dahulu");
            }
            else if (jumlah <= barang.Jumlah && jumlah >= 0)
            {
                var harga = barang.harga;
                Modelbarangretur item = new Modelbarangretur();

                item.Id_Barang = barang.Id_Barang;
                item.Jumlah = jumlah;
                item.Nama_Barang = barang.Nama_Barang;
                // item.Potongan = int.Parse(potongan);
                item.Harga = barang.harga;
                datanota.Add(item);
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = datanota;

                // txtttotal.Text = datanota.Sum(i => i.Sub_Total).ToString();
                txttotal.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));
            }
            else
            {
                MessageBox.Show("jumlah yang anda masukan tidak valid");
            }
            // var potongan = txtpotongan.Text;

        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            try
            {
                BengkelEntities5 context = new BengkelEntities5();
                Nota_Retur_Pembelian_barang noret = new Nota_Retur_Pembelian_barang();
                if (cbonota.SelectedIndex==-1)
                {
                    MessageBox.Show("PILIH NOTA PEMBELIAN TERLEBIH DAHULU");
                }
               if(txtketerangan.Text=="")
                {
                    MessageBox.Show("ISI TERLEBIH DAHULU KETERANGAN");
                    txtketerangan.Focus();
                }
               else
                {
                    noret.bengkel_id = username.Bengkel_id;
                    noret.Id_retur_Beli = lblretur.Text;
                    noret.Tanggal_beli = DateTime.Now;
                    noret.Nomor_Nota_Beli =  cbonota.SelectedItem.ToString();
                    noret.Keterangan = txtketerangan.Text;
                    noret.suplier_id = lblspl.Text;
                    MessageBox.Show("Data Berhasil di Simpan");
                    noret.total = datanota.Sum(i => i.Sub_Total);
                    context.SaveChanges();
                }
                context.Nota_Retur_Pembelian_barang.Add(noret);
                foreach (var item in datanota)
                {
                    detail_nota_retur_pembelian denor = new detail_nota_retur_pembelian();
                    denor.id_retur_nota_beli = noret.Id_retur_Beli;
                    denor.id_barang = item.Id_Barang;
                    denor.jumlah = item.Jumlah;
                    denor.harga = item.Harga;
                    denor.nama_barang = item.Nama_Barang;
                    denor.subtotal = item.Sub_Total.ToString();
                    context.detail_nota_retur_pembelian.Add(denor);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Isi Sesuai Dengan Retur Pembelian");
            }
        }
        private void btncek_Click(object sender, EventArgs e)
        {   
            string bdd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection konn = new SqlConnection(bdd);
            string sql3 = "select b.Bengkel_id,b.Id_Barang,b.Nama_Barang,d.jumlah,d.Harga from Detail_nota_pembelian as d inner join Barang b on b.Id_Barang = d.barang_id and b.Tipe_barang='1' where d.id_nota_pembelian=@idnota;";
            konn.Open();
            if(cbonota.SelectedIndex==-1)
            {
                MessageBox.Show("Pilih Nota Pembelian");
            }
            else
            {
                cbvalue dt = (cbvalue)cbonota.SelectedItem;
                List<modelnotajuallll> data = konn.Query<modelnotajuallll>(sql3, new { idnota = dt.Id_nota_pembelian }).ToList();

                lblspl.Text = dt.Nama_Suplier;

                // MessageBox.Show(dt.Nama_Suplier);
                cbobarang.Items.Clear();
                // cbobarang.SelectedIndex = int.Parse(lblspl.Text.ToString());
                foreach (var item in data)
                {
                    cbobarang.Items.Add(item);
                }
            }
           
           
        }
        private void Retur_Belii_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }

        private void cbonota_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
    }

}
