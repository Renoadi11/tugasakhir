﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Bengkel.Model;

namespace Bengkel
{
    public partial class Masterbengkel : Form
    {
        Karyawan username;
        utama ut;
        public Masterbengkel(Karyawan username, utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            load();

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            formTambahMaster masterr = new formTambahMaster(username);
            masterr.Show();
        }
        private void load()
        {

            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select  Id_Bengkel, nama ,Alamat,telepon,logo from Master_Bengkel where Id_Bengkel=@id" ;
            List<Masterbengkellll> data = kon.Query<Masterbengkellll>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();

            dataGridView1.DataSource = data;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            Upmasterbengkel tambah = new Upmasterbengkel (up, username);
            tambah.Show();

        }

        private void Masterbengkel_Load(object sender, EventArgs e)
        {
            ut.Show();
        }
    }
}
