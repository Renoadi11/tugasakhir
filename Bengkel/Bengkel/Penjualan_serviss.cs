﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;


namespace Bengkel
{
    public partial class Penjualan_serviss : Form
    {
        int sub = 0;
        int totall = 0;
        Karyawan username;
        modelmekanik Mek;
        utama ut;
        
        public List<Modelbarangservis> datanota1 = new List<Modelbarangservis>();
        public List<ModelBarangNota> datanota2 = new List<ModelBarangNota>();
        public string kodeboking = null;

        List<ModelJasanota> datser = new List<ModelJasanota>();
        
        public Penjualan_serviss(Karyawan username, utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            defaultform_load();
            lbladmin.Text = username.Nama;
            load();
        }
     //   public string kodeboking { get; set; }
        public void Changelabel(string data)
        {
            label5.Text = data;
        }


        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }
        public void load()
        {
            this.vw_notajualTableAdapter.FillBy(this.bengkelDataSet4.vw_notajual, "KRT1");
            BengkelEntities5 context = new BengkelEntities5();
            var ben = (from bk in context.Master_Bengkel where bk.Id_Bengkel == username.Bengkel_id select bk).FirstOrDefault();
            if (ben.Diskon == 0)
            {
                //txtpotongan.Hide();
                txtpottongan.Hide();
                txtpot.Hide();
            }
            if(ben.Booking==0)
            {
                btndiskon.Hide();
            }
                 

        }
        public string get_idNotservis()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select CONCAT(YEAR( GETDATE()),RIGHT('00' + CONVERT(VARCHAR(12),  MONTH(GETDATE())), 2),RIGHT('0000' + CONVERT(VARCHAR(12), next value for Getidnotaservis ), 4)) id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "SO/" + id;
        }
        public void defaultform_load()
        {
            //this.vw_notajualTableAdapter.FillBy(this.bengkelDataSet2.vw_notajual, username.Bengkel_id);

            cboservis.Items.Clear();
            cbobarang.Items.Clear();
            timer1.Enabled = true;
           
            // cbokendaraan.Items.Clear();
            lblnota.Text = get_idNotservis();
            lbltgl.Text = DateTime.Now.ToString();

            
            string bdd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection konn = new SqlConnection(bdd);
            konn.Open();
         //   string sql = "select Id_Barang, Nama_Barang,Jenis_Barang,Harga_Satuan,Harga_Beli,No_rak_barang,Stok_Dimiliki,Stok_minimal,Status_barang,penambahan_Stok from Barang where Bengkel_id=@id and Tipe_Barang =@tp ;";
          //  string sql2 = "select Kode_Kendaraan, Pemilik, merk,Tipe,Kilometer from Kendaraan where Bengkel_id  =@id;";
            string sql3 = "select Id_Barang as IdServis, Nama_Barang  as NamaServis,Jenis_Barang,Harga_Satuan,Harga_Beli,No_rak_barang,Stok_Dimiliki,Stok_minimal,Status_barang,penambahan_Stok from Barang where Bengkel_id =@id and Tipe_Barang =@tp and Hapus=0 ;";
            string sql2 = "select Id_Barang, Nama_Barang,Jenis_Barang,Harga_Satuan,Harga_Beli,No_rak_barang,Stok_Dimiliki,Stok_minimal,Status_barang,penambahan_Stok from Barang where Bengkel_id =@id and Tipe_Barang =@tp and Hapus=0;";
            string sql4 = "select * from Status_servis as s where s.id_status_penjualan=3;";
            string sql6 = "select Id_Mekanik,nama,Alamat,telepon from Mekanik where bengkel_id = @id";
            string sql5 = "select *from Nota_jual_barang as b inner join Status_servis as sv on sv.id_status_penjualan =b.status_id where b.status_id != 3";
            List<ModelBarang> data = konn.Query<ModelBarang>(sql2, new { id = username.Bengkel_id, tp="1" }).ToList();
          //  List<modelkendaraan> data2 = konn.Query<modelkendaraan>(sql2, new { id = username.Bengkel_id }).ToList();
            List<modelserviss> data3 = konn.Query<modelserviss>(sql3,new { id = username.Bengkel_id, tp="2" }).ToList();
            List<Modelsatus> data4 = konn.Query<Modelsatus>(sql4).ToList();
            List<modelmekanik> data2 = konn.Query<modelmekanik>(sql6, new { id = username.Bengkel_id }).ToList();

            BengkelEntities5 conte = new BengkelEntities5();
            var jumlah = (from x in conte.Bookings where x.bengekl_id==username.Bengkel_id && x.Status_id == 1 select x).ToList();
            lblbooking.Text = jumlah.Count().ToString()+" Booking";




            konn.Close();
            foreach (var item in data2)
            {
                cbomek.Items.Add(item);
            }
            // dataGridView1.DataSource = data;
            foreach (var item in data)
            {
                cbobarang.Items.Add(item);
            }
            //foreach (var item in data2)
            //{
            //    cbokendaraan.Items.Add(item);
            //}
            foreach (var item in data3)
            {
               cboservis.Items.Add(item);
            }
            foreach (var item in data4)
            {
                cbostatus.Items.Add(item);
            }
           // this.nota_jual_barangTableAdapter.FillBy1(this.bengkelDataSet, username.Bengkel_id, new System.Nullable<int>(((int)(System.Convert.ChangeType(1, typeof(int))))));
            DataGridViewLinkColumn Editlink = new DataGridViewLinkColumn();
            Editlink.UseColumnTextForLinkValue = true;
            Editlink.Text = "Cetak";
            advancedDataGridView1.Columns.Add(Editlink);
           // this.nota_jual_barangTableAdapter.FillBy(this.bengkelDataSet1.Nota_jual_barang);


        }
        public void resetdatagridview()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = datanota1;

            dataGridView2.DataSource = null;
            dataGridView2.DataSource = datanota2;

            int total1 = datanota1.Sum(i => i.SubTotal);
            int total2 = datanota2.Sum(i => i.SubTotal);

        //    lblboking = kodeboking;
          //  MessageBox.Show(this.kodeboking);
            txttotal.Text = total1.ToString();
            txttottal.Text = total2.ToString();
            txtsub.Text = (total1 + total2).ToString();
            // MessageBox.Show("Test");
            // txttotal.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));
            //txtbayar.Text = datanota2.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));

            total();
        }
       

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ModelBarang barang = (ModelBarang)cbobarang.SelectedItem;
            var jumlah = txtjumlah.Text;
            var potongan =txtpot.Text;
            // var harga = barang.Harga_Satuan;
            button5.Enabled = true;
            button6.Enabled = true;

            ModelBarangNota item = new ModelBarangNota();

            if(cbobarang.SelectedIndex==-1)
            {
                MessageBox.Show("Pilih Barang Terlebih Dahulu");
                
            }
            else
            {
                item.IdBarang = barang.Id_Barang;
                item.Jumlah = int.Parse(jumlah);
                item.NamaBarang = barang.Nama_Barang;
                item.Potongan = int.Parse(potongan);
                item.Harga = barang.Harga_Satuan;

                datanota2.Add(item);
                dataGridView2.DataSource = null;
                dataGridView2.DataSource = datanota2;

                txttottal.Text = datanota2.Sum(i => i.SubTotal).ToString();
                // txttotal.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));
                // txtsisa.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));

                total();
            }


           

        }
        public void total()
        {
            int total1 = datanota1.Sum(i => i.SubTotal);
            int total2 = datanota2.Sum(i => i.SubTotal);
            txtsub.Text = (total1 + total2).ToString("C", new CultureInfo("id-ID"));
            totall = total1 + total2;
        }
        
        private void button5_Click(object sender, EventArgs e)
        {
            ModelBarangNota barnot = (ModelBarangNota)dataGridView2.CurrentRow.DataBoundItem;
            Editpenjualanbarangservis ed = new Editpenjualanbarangservis(barnot,this);
            ed.Show();

        }
        public string get_iddetailnota()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIddetailnotajual id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "DT" + id;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
               
                BengkelEntities5 context = new BengkelEntities5();
                Nota_jual_barang not = new Nota_jual_barang();

                
                int total1 = datanota1.Sum(i => i.SubTotal);
                int total2 = datanota2.Sum(i => i.SubTotal);
                Mek = (modelmekanik)cbomek.SelectedItem;
                Modelsatus status = (Modelsatus)cbostatus.SelectedItem;
                not.Id_nota_Jual = get_idNotservis();
              //  not.mekanik_id = Mek.Id_Mekanik;
                not.Karyawan_Id = username.Id_Karyawan;
                not.Bengkel_id = username.Bengkel_id;
                not.Tanggal_Jual = DateTime.Now;
                not.Total = totall;
                not.Keterangan = txtketera.Text;
               
                if (cbomek.SelectedIndex > -1)
                {
                    not.mekanik_id = Mek.Id_Mekanik;
                }
                //if(cbostatus.SelectedIndex==-1)
                //{
                //    MessageBox.Show("ISI DULU STATUS");
                //}
                
                if(txtsisa.Text!="")
                {
                    int bayar = int.Parse(txtbayar.Text);
                    not.kembalian =(bayar - (total1+total2));
                }
                not.jumlah_bayar= int.Parse(txtbayar.Text);
                
                //if(txtpotongan.Text!="")
                //{
                //    not.potongan = int.Parse(txtpotongan.Text);
                //}
                if(cbostatus.SelectedIndex>0)
                {
                    not.status_id = status.id_status_penjualan;
                }


                context.Nota_jual_barang.Add(not);

                if(ckbarang.Checked ==true)
                {

                    foreach (var item in datanota2)
                    {

                        BengkelEntities5 conte = new BengkelEntities5();
                        Detail_Jual_Barang det = new Detail_Jual_Barang();
                        det.Id_detail_nota_jual = get_iddetailnota();
                        det.id_nota_jual = not.Id_nota_Jual;
                        det.id_barang = item.IdBarang;
                        det.Nama_Barang = item.NamaBarang;
                        det.Harga = item.Harga;
                        det.potongan = item.Potongan;
                        det.Jumlah = item.Jumlah;
                        det.Sub_total = item.SubTotal;
                        

                        context.Detail_Jual_Barang.Add(det);


                    }
                    MessageBox.Show("Data Berhasil di Simpan");
                }

                
                if (ckservis.Checked == true)
                {
                    foreach (var item in datanota1)
                    {

                        BengkelEntities5 conte = new BengkelEntities5();
                        Detail_Jual_Barang det = new Detail_Jual_Barang();
                        det.Id_detail_nota_jual = get_iddetailnota();
                        det.id_nota_jual = not.Id_nota_Jual;
                        det.id_barang = item.IdServis;
                        det.Nama_Barang = item.NamaServis;
                        det.Harga = item.Harga;
                        det.potongan = item.Potongan;
                       // det.Jumlah = item.Jumlah;
                        det.Sub_total = item.SubTotal;
                        context.Detail_Jual_Barang.Add(det);
                    }
                    
                }
                if(label5.Text != null && label5.Text!="")
                {
                    Booking bok = context.Bookings.Where(c => c.Id_Booking == label5.Text).FirstOrDefault();
                    bok.nota_id = not.Id_nota_Jual;
                    bok.Status_id = 3;
                    context.SaveChanges();

                    Formlaporanjual jual = new Formlaporanjual(username, not);
                    jual.Show();

                }

                if (context.SaveChanges()>1)
                {
                    Formlaporanjual jual = new Formlaporanjual(username,not);
                    jual.Show();
                }
                this.vw_notajualTableAdapter.FillBy(this.bengkelDataSet4.vw_notajual, username.Bengkel_id);

                MessageBox.Show("PESANAN TELAH DISIMPAN");

            }
            catch (Exception ex)
            {

                MessageBox.Show("Isi Sesuai Dengan Transaksi Penjualan");
            }

        }

        private void lbladmin_Click(object sender, EventArgs e)
        {

        }

        private void btntambah_Click(object sender, EventArgs e)
        {
            FormtambahKendaraan tamb = new FormtambahKendaraan(username,this);
                tamb.Show();

        }

        private void cbokendaraan_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            //if (cbokendaraan.SelectedIndex > -1)
            //{
            //    modelkendaraan kendaraan = (modelkendaraan)cbokendaraan.SelectedItem;
            //    lblkilometer.Text = kendaraan.Kilometer.ToString();
            //    lblnamapemilik.Text = kendaraan.Pemilik.ToString();
            //    lbltipe.Text = kendaraan.Tipe.ToString();
            //    lblmerk.Text = kendaraan.merk.ToString();
            //    cboservis.Enabled = true;
            //    txtpottongan.Enabled = true;
            //    button1.Enabled = true;
            //}
            //else
            //{
            //    cboservis.Enabled = false;
            //    txtpottongan.Enabled = false;
            //    button1.Enabled = false;
            //}
        }

        private void Penjualan_serviss_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            modelserviss jasa = (modelserviss) cboservis.SelectedItem;
            var jumlah = textBox4.Text;
            var pottongan = txtpottongan.Text;
            //var harga = jasa.Harga_Satuan;
            Modelbarangservis item = new Modelbarangservis();

            if (cboservis.SelectedIndex == -1)
            {
                MessageBox.Show("Pilih Nama Servis");
            }
            

            else
            {
                item.IdServis = jasa.IdServis;
                //  item.Jumlah = int.Parse(jumlah);
                item.NamaServis = jasa.NamaServis;
                item.Potongan = int.Parse(pottongan);
                item.Harga = jasa.Harga_Satuan;

                button3.Enabled = true;

                datanota1.Add(item);
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = datanota1;

                txttotal.Text = datanota1.Sum(i => i.Harga).ToString();


                total();
            }

           
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ModelBarangNota barnot = (ModelBarangNota)dataGridView2.CurrentRow.DataBoundItem;
            datanota2.RemoveAll(r => r.IdBarang == barnot.IdBarang);
            resetdatagridview();
            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Modelbarangservis da = (Modelbarangservis)dataGridView1.CurrentRow.DataBoundItem;
            datanota1.RemoveAll(r =>r.IdServis == da.IdServis);
            resetdatagridview();

        }

        private void Penjualan_serviss_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bengkelDataSet4.vw_notajual' table. You can move, or remove it, as needed.
            //this.vw_notajualTableAdapter.Fill(this.bengkelDataSet4.vw_notajual);
            this.vw_notajualTableAdapter.FillBy(this.bengkelDataSet4.vw_notajual, username.Bengkel_id);

            this.bookingTableAdapter.Fill(this.bengkelDataSet.Booking);

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void ckservis_CheckedChanged(object sender, EventArgs e)
        {
            if(ckservis.Checked == false)
            { 
                 cboservis.Enabled = false;
                 txtpottongan.Enabled = false;
                 button1.Enabled = false;
                 textBox4.Enabled = false;
                button9.Enabled = false;
                 dataGridView1.Enabled = false;
                txtbayar.Enabled = false;
                cbomek.Enabled = false;
            }
            else
            {
                cboservis.Enabled =true;
                txtpottongan.Enabled = true;
                button1.Enabled = true;
                textBox4.Enabled = true;
                button9.Enabled = true;
                dataGridView1.Enabled = true;
                txtbayar.Enabled = true;
                cbomek.Enabled = true;
            }
        }

        private void ckbarang_CheckedChanged(object sender, EventArgs e)
        {
            if(ckbarang.Checked == false)
            {
                cbobarang.Enabled = false;
                txtjumlah.Enabled = false;
                txtpot.Enabled = false;
                txtbayar.Enabled = false;
                button4.Enabled = false;
                button9.Enabled = false;

                dataGridView2.Enabled = false;
            }
            else
            {
                cbobarang.Enabled = true;
                txtjumlah.Enabled = true;
                txtpot.Enabled = true;
                button4.Enabled = true;
                button9.Enabled = true;
                txtbayar.Enabled = true;
                dataGridView2.Enabled = true;
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtbayar_KeyUp(object sender, KeyEventArgs e)
        {
            if(txtsub.Text !="")
            {
                int total1 = datanota1.Sum(i => i.SubTotal);
                int total2 = datanota2.Sum(i => i.SubTotal);
                int total = total1 + total2;
                int bayar = int.Parse(txtbayar.Text);
                if (bayar > 0)
                {
                    txtsisa.Text = (bayar - total).ToString();
                    txtsisa.Text = (bayar - total).ToString("C", new CultureInfo("id-ID"));
                    // if(total =! null && bayar)
                }
                else
                {
                    MessageBox.Show("Jumlahnya Kurang");
                }

            }
        }

        private void cboservis_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            tes open = new tes(username,this);
            open.Show();

        }

        private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label3_Click_1(object sender, EventArgs e)
        {

        }

        private void txtbayar_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void txtpotongan_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void txtpot_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void txtjumlah_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void txtpottongan_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void btnup_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (label6.Left < 820)
            {
                label6.Left = label6.Left + 4;
            }
            else
            {
                label6.Left = label6.Left - 1000;
            }
        }

        private void advancedDataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
               // this.vw_notajualTableAdapter.FillBy(this.bengkelDataSet2.vw_notajual, username.Bengkel_id);
                string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
                SqlConnection kon = new SqlConnection(bd);
                string id = advancedDataGridView1[0, e.RowIndex].Value.ToString();
                kon.Open();
                string sql2 = "select *from Nota_jual_barang  as b where b.Id_nota_Jual=@id and b.Bengkel_id=@idbengkel;";
                Nota_jual_barang data = kon.Query<Nota_jual_barang>(sql2, new { id=id, idbengkel=username.Bengkel_id}).FirstOrDefault();
                Formlaporanjual jual = new Formlaporanjual(username, data);
                jual.Show();
                resetdatagridview();
                this.Close();


            }
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                //this.vw_notajualTableAdapter.FillBy(this.bengkelDataSet2.vw_notajual,username.Bengkel_id);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillByToolStripButton_Click_1(object sender, EventArgs e)
        {
            try
            {
               // this.nota_jual_barangTableAdapter.FillBy(this.bengkelDataSet1.Nota_jual_barang);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void vwnotajualBindingSource_ListChanged(object sender, ListChangedEventArgs e)
        {

        }

        private void fillByToolStripButton_Click_2(object sender, EventArgs e)
        {
            try
            {
                //this.vw_notajualTableAdapter1.FillBy(this.bengkelDataSet3.vw_notajual, idToolStripTextBox.Text);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillByToolStripButton_Click_3(object sender, EventArgs e)
        {
            try
            {
               // this.vw_notajualTableAdapter.FillBy(this.bengkelDataSet4.vw_notajual, paramToolStripTextBox.Text);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }
    }
}
