﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class upmekanik : Form
    {
        Karyawan username;
        Mekanik mek;
        string id;
        Mekanikk meka;
        BengkelEntities5 upp = new BengkelEntities5();
        public upmekanik(string id, Karyawan username, Mekanikk meka)
        {
            this.meka = meka;
            this.username = username;
            InitializeComponent();
            this.id = id;
            load();
        }
        public void load()
        {
            var data1 = (from x in upp.Mekaniks where x.Id_Mekanik == id select x).FirstOrDefault<Mekanik>();
            txtnama.Text = data1.Nama;
            txtalamat.Text = data1.Alamat;
            txttelpon.Text = data1.telepon;

            mek = data1;
        }

        private void upmekanik_Load(object sender, EventArgs e)
        {

        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            mek.Nama = txtnama.Text;
            mek.Alamat = txtalamat.Text;
            mek.telepon = txttelpon.Text;
            MessageBox.Show("Data Berhasil Diubah...");
            upp.SaveChanges();
        }

        private void upmekanik_FormClosing(object sender, FormClosingEventArgs e)
        {
            meka.load();
        }
    }
}
