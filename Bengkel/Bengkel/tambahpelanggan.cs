﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;
using System.Configuration;

namespace Bengkel
{
    public partial class tambahpelanggan : Form
    {
        public tambahpelanggan()
        {
            InitializeComponent();
            defaultform_load();
        }
        public string get_IdPelanggan()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetidPelanggan id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "PL" + id;

        }
        public void defaultform_load()
        {
            txtid.Text = get_IdPelanggan();

        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            try
            {
                BengkelEntities5 context = new BengkelEntities5();
                Pelanggan data = cekvalidasi();
                if (data != null)
                {
                    using (var ctx = new BengkelEntities5())
                    {
                        ctx.Pelanggans.Add(data);
                        ctx.SaveChanges();
                    }
                    MessageBox.Show("Data berhasil ditambah ...");

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public Pelanggan cekvalidasi()
        {
            string id = txtid.Text;
            string nama = txtnama.Text;
            string alamat = txtalamat.Text;
            string telpon = txttelepon.Text;
            if (rdolaki.Checked)
            {
                string Jenis_Kelamin = rdolaki.Text;

            }
            else
            {
                string Jenis_Kelamin = rdoperempuan.Text;

            }
            if (rdoya.Checked)
            {
                string Sudah_diingatkan = rdoya.Text;

            }
            else
            {
                string Sudah_diingatkan = rdobelum.Text;

            }
            String Tanggal_Terakhir_Servis = dateTimePicker1.Value.ToString();


            //    clear();
            




            Pelanggan pela = new Pelanggan();
            pela.Id_pelanggan = id;
            pela.Nama_pelanggan = nama;
            pela.Alamat_Pelanggan = alamat;
            pela.Telpon = telpon;
            if (rdolaki.Checked)
            {
                pela.Jenis_Kelamin = rdolaki.Text;
                MessageBox.Show(rdolaki.Text);
            }
            else
            {
                pela.Jenis_Kelamin = rdoperempuan.Text;
                MessageBox.Show(rdoperempuan.Text);
            }
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "yyyy-mm-dd h:m:s";
            MessageBox.Show(dateTimePicker1.Value.ToString("yyyy-mm-dd h:m:s"));
            pela.Tanggal_Terakhir_Servis = dateTimePicker1.Value;
            if (rdoya.Checked)
            {
                pela.Sudah_diingatkan = rdoya.Text;
                MessageBox.Show(rdoya.Text);
            }
            else
            {
                pela.Sudah_diingatkan = rdobelum.Text;
                MessageBox.Show(rdobelum.Text);
            }
            return pela;

        }

        private void txttelpon_TextChanged(object sender, EventArgs e)
        {

        }

        private void txttelepon_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
