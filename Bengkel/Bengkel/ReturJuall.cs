﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;

namespace Bengkel
{
    public partial class ReturJuall : Form
    {
        Karyawan username;
        utama ut;
        public List<Modelbarangretur> datanota = new List<Modelbarangretur>();
        public ReturJuall(Karyawan username, utama ut)
        {
            this.username = username;
            this.ut = ut;
            InitializeComponent();
            defaultform_load();
            lbladmin.Text = username.Nama;
        }
        public string get_idreturjual()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select CONCAT(YEAR( GETDATE()),RIGHT('00' + CONVERT(VARCHAR(12),  MONTH(GETDATE())), 2),RIGHT('0000' + CONVERT(VARCHAR(12), next value for GetIdReturjual ), 4)) id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "RTU" + id;
        }
        public void defaultform_load()
        {
            //cboservis.Items.Clear();
            //cbobarang.Items.Clear();
            //cbokendaraan.Items.Clear();
            lblretur.Text = get_idreturjual();
            lbltgl.Text = DateTime.Now.ToString();

            string bdd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection konn = new SqlConnection(bdd);
           // string sql = "select Id_Barang, Nama_Barang,Jenis_Barang,Harga_Satuan,Harga_Beli,No_rak_barang,Stok_Dimiliki,Stok_minimal,Status_barang,penambahan_Stok from Barang where Bengkel_id=@id;";
            string sql2 = "select Id_nota_Jual from Nota_jual_barang n where convert(date, Tanggal_Jual) = convert (date, getdate()) and Bengkel_id=@id";
           // List<ModelBarang> data = konn.Query<ModelBarang>(sql, new { id = username.Bengkel_id }).ToList();
            List<string> data2 = konn.Query<string>(sql2,new { id = username.Bengkel_id }).ToList();
            konn.Close();
            //foreach (var item in data)
            //{
            //    cbobarang.Items.Add(item);
            //}
            foreach (var item in data2)
            {
                cbonota.Items.Add(item);
            }
        }
        public void resetdatagridview()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = datanota;

           // txtttotal.Text = datanota.Sum(i => i.Sub_Total).ToString();
            txttotal.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));
           // txtsisa.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                BengkelEntities5 context = new BengkelEntities5();
                Nota_retur_Jual_barang noret = new Nota_retur_Jual_barang();
               if(cbonota.SelectedIndex==-1)
                {
                    MessageBox.Show("PILIH NOMOR NOTA PENJUALAN TERLEBIH DAHULU");
                }
               if(txtketerangan.Text=="")
                {
                    MessageBox.Show("ISI TELEBIH DAHULU KETERANGAN");
                    txtketerangan.Focus();
                }
               else
                {
                    noret.Bngkel_id = username.Bengkel_id;
                    noret.Nota_Retur_Jual_Id = lblretur.Text;
                    noret.tanggal_retur = DateTime.Now;
                    noret.No_Nota_Jual = cbonota.SelectedItem.ToString();
                    noret.Keterangan = txtketerangan.Text;
                    MessageBox.Show("Data Berhasil di Simpan");
                    noret.total = datanota.Sum(i => i.Sub_Total);
                    context.Nota_retur_Jual_barang.Add(noret);
                    context.SaveChanges();
                }
                foreach (var item in datanota)
                {
                    Detail_Retur_Jual denor = new Detail_Retur_Jual();
                    denor.id_retur_nota = noret.Nota_Retur_Jual_Id;                 
                    denor.id_barang = item.Id_Barang;
                    denor.jumlah = item.Jumlah;
                    denor.harga = item.Harga;
                    denor.nama_barang = item.Nama_Barang;
                    denor.sub_total = item.Sub_Total.ToString();
                    context.Detail_Retur_Jual.Add(denor);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void btntambah_Click(object sender, EventArgs e)
        {

            int jumlah = int.Parse(txtjumlah.Text);
            modelnotajuallll barang = (modelnotajuallll)cbobarang.SelectedItem;
            if(cbobarang.SelectedIndex ==-1)
            {
                MessageBox.Show("Pilih Barang Terlebih dahulu");
            }

             else if(jumlah <= barang.Jumlah && jumlah >= 0)
            {
                var harga = barang.harga;

                Modelbarangretur item = new Modelbarangretur();


                item.Id_Barang = barang.Id_Barang;
                item.Jumlah = jumlah;
                item.Nama_Barang = barang.Nama_Barang;
                // item.Potongan = int.Parse(potongan);
                item.Harga = barang.harga;





                datanota.Add(item);
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = datanota;

                // txtttotal.Text = datanota.Sum(i => i.Sub_Total).ToString();
                txttotal.Text = datanota.Sum(i => i.Sub_Total).ToString("C", new CultureInfo("id-ID"));
            }
             else
            {

                MessageBox.Show("jumlah yang anda masukan tidak valid");
            }
            // var potongan = txtpotongan.Text;
         
        }

        private void ReturJuall_Load(object sender, EventArgs e)
        {
            ut.Show();
        }

        private void txttotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void btncek_Click(object sender, EventArgs e)
        {

            string bdd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection konn = new SqlConnection(bdd);
            string sql3 = "select b.Id_Barang ,b.Nama_Barang,d.Jumlah,d.Harga  from Detail_Jual_Barang d inner join Barang b on b.Id_Barang = d.id_barang and b.Tipe_barang='1' where d.id_nota_jual=@idnota";
            konn.Open();
            if(cbonota.SelectedIndex==-1)
            {
                MessageBox.Show("Pilih Nota Pembelian");
            }
            else
            {
                List<modelnotajuallll> data = konn.Query<modelnotajuallll>(sql3, new { idnota = cbonota.SelectedItem }).ToList();

                cbobarang.Items.Clear();
                foreach (var item in data)
                {

                    cbobarang.Items.Add(item);
                }
            }
           
            
        }
        public void clear()
        {
            
        }

        private void cbobarang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbobarang.SelectedIndex >-1)
            {
                modelnotajuallll mod = (modelnotajuallll) cbobarang.SelectedItem;
                txtjumlah.Text = mod.Jumlah.ToString();
            }
        }

        private void ReturJuall_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }

        //private void txtbayar_KeyUp(object sender, KeyEventArgs e)
        //{
        //    if (txttotal.Text != "" && txtbayar.Text != "")
        //    {
        //       int total = int.Parse(txttotal.Text);
        //        int bayar = int.Parse(txtbayar.Text);
        //        if (bayar > 0)
        //        {
        //            //txtsisa.Text = (bayar - total).ToString();
        //            txtsisa.Text = (bayar - total).ToString("C", new CultureInfo("id-ID"));
        //            // if(total =! null && bayar)
        //        }


        //    }
        //    else
        //    {
        //        txtsisa.Text = null;
        //       // txtsisa.Text = null;
        //    }

        //}
    }
}
