﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class LaporanPenjualan : Form
    {
        Karyawan username;
        utama ut;
        string NAMA;
        public LaporanPenjualan(Karyawan username, utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            
        }

        private void btnjual_Click(object sender, EventArgs e)
        {
            formlaporancs lapor = new formlaporancs(username);
            lapor.Hide();
            lapor.Show();

        }

        private void LaporanPenjualan_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }

        private void btnser_Click(object sender, EventArgs e)
        {
            FormLaporanservis lapo = new FormLaporanservis(username,ut);
            lapo.Hide();
            lapo.Show();
        }
    }
}
