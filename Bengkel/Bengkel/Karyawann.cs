﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using System.Data.SqlClient;
using System.Configuration;
using Bengkel.Model;

namespace Bengkel
{
    public partial class Karyawann : Form
    {
        Karyawan username;
        utama ut;
        public Karyawann(Karyawan username, utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            load();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            formtambahKaryawan tam = new formtambahKaryawan(username,this);
            tam.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            UpKaryawan tambah = new UpKaryawan(up, username, this);
            tambah.Show();


        }
        public void load()
        {
            dataGridView1.DataSource = null;
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Karyawan, Nama,Alamat,Telpon,[Kata sandi],Bengkel_id from Karyawan where Bengkel_id=@id";
            List<ModelKaryawan> data = kon.Query<ModelKaryawan>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();

            dataGridView1.DataSource = data;
        }

        private void Karyawann_Load(object sender, EventArgs e)
        {
            ut.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            var hapus = (from c in context.Karyawans where c.Id_Karyawan == up select c).FirstOrDefault();
            context.Karyawans.Remove(hapus);
            context.SaveChanges();
            MessageBox.Show("Berhasil Dihapus");
            load();
        }



        //private void button5_Click(object sender, EventArgs e)
        //{
        //    BengkelEntities5 context = new BengkelEntities5();
        //    var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
        //    var hapus = (from c in context.Karyawans where c.Id_Karyawan == up select c).FirstOrDefault();
        //    hapus.Hapus = 1;
        //    context.SaveChanges();
        //    MessageBox.Show("Berhasil Dihapus");
        //    load();
        //}
    }
}
