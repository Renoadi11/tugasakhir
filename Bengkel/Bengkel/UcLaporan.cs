﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bengkel
{
    public partial class UcLaporan : UserControl
    {
        utama ut;
        Karyawan ky;
        public UcLaporan(utama ut, Karyawan ky)
        {
            this.ut = ut;
            this.ky = ky;
            InitializeComponent();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            LaporanPenjualan tampil = new LaporanPenjualan(ky, ut);
            ut.Hide();
            tampil.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            laporanbarang lap = new laporanbarang(ky);
            lap.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            Formlaporanpembelian lapo = new Formlaporanpembelian(ky,ut );
            lapo.Hide();
            lapo.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Formlaporanretujual lapo = new Formlaporanretujual(ky);
            lapo.Hide();
            lapo.Show();

        }

        private void button9_Click(object sender, EventArgs e)
        {
            Formlaporanreturbeli lapo = new Formlaporanreturbeli(ky, ut);
            lapo.Hide();
            lapo.Show();
        }
    }
}
