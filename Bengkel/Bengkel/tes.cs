﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Dapper;
using System.Data.SqlClient;
using Bengkel.Model;
using System.Globalization;

namespace Bengkel
{
    public partial class tes : Form
    {
        Karyawan username;
        Penjualan_serviss ps;
       
        public tes(Karyawan username, Penjualan_serviss ps)
        {
            
            this.ps = ps;
            this.username = username;
            InitializeComponent();


          //  defaultform_load();

        }

        private void tes_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bengkelDataSet.Booking' table. You can move, or remove it, as needed.
            //this.bookingTableAdapter.Fill(this.bengkelDataSet.Booking);
            this.bookingTableAdapter.FillBy1(this.bengkelDataSet.Booking, username.Bengkel_id, new System.Nullable<int>(((int)(System.Convert.ChangeType(1, typeof(int))))));
            DataGridViewLinkColumn Editlink = new DataGridViewLinkColumn();
            Editlink.UseColumnTextForLinkValue = true;
            Editlink.Text = "Proses";
            advancedDataGridView1.Columns.Add(Editlink);

            DataGridViewLinkColumn detaillink = new DataGridViewLinkColumn();
            detaillink.UseColumnTextForLinkValue = true;
            detaillink.Text = "Detail";
            advancedDataGridView1.Columns.Add(detaillink);

            DataGridViewLinkColumn hapuslink = new DataGridViewLinkColumn();
            hapuslink.UseColumnTextForLinkValue = true;
            hapuslink.Text = "Batal";
            advancedDataGridView1.Columns.Add(hapuslink);

            advancedDataGridView1.EnableFilterAndSort(advancedDataGridView1.Columns[1]);
        }
        public void defaultform_load()
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            // string sql = "select * from Booking where Status_id= 1 and convert(date, Tanggal_booking) = convert (date, getdate()) and bengekl_id=@id;";
            string sql = "select * from Booking where Status_id= 1 and bengekl_id=@id;";
            List<Bookinggg> data = kon.Query<Bookinggg>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            
            advancedDataGridView1.DataSource = null;
            advancedDataGridView1.SetDoubleBuffered();
            advancedDataGridView1.DataSource = data;

            DataGridViewLinkColumn Editlink = new DataGridViewLinkColumn();
            Editlink.UseColumnTextForLinkValue = true;
            Editlink.Text = "Proses";
            advancedDataGridView1.Columns.Add(Editlink);

            DataGridViewLinkColumn hapuslink = new DataGridViewLinkColumn();
            hapuslink.UseColumnTextForLinkValue = true;
            hapuslink.Text = "Batal";
            advancedDataGridView1.Columns.Add(hapuslink);

        
        }


        private void advancedDataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            kon.Close();
            MessageBox.Show(e.ColumnIndex.ToString());
            if (e.ColumnIndex == 7)
            {

                string data = advancedDataGridView1[0, e.RowIndex].Value.ToString();
               // MessageBox.Show(data);
                kon.Open();
                string sql = "select db.id_barang as IdBarang, db.jumlah as Jumlah, db.nama_barang as NamaBarang, db.potongan as Potongan, db.harga as Harga from Detail_booking as db inner join barang as b on b.Id_Barang = db.id_barang where b.Tipe_barang =@tp and db.Booiking_id=@idboking;";
                string sql1 = "select db.id_barang  as IdServis, db.jumlah as Jumlah, db.nama_barang as NamaServis, db.potongan as Potongan, db.harga as Harga from Detail_booking as db inner join barang as b on b.Id_Barang = db.id_barang where b.Tipe_barang =@tp and db.Booiking_id=@idboking;";
                string sql2 = " select *from Booking as b where b.Id_Booking=@idboking";
                List<ModelBarangNota> data1 = kon.Query<ModelBarangNota>(sql, new { idboking = data, tp = "1" }).ToList();
                List<Modelbarangservis> data2 = kon.Query<Modelbarangservis>(sql1, new { idboking = data, tp = "2" }).ToList();
                Bookinggg data3= kon.Query<Bookinggg>(sql2, new { idboking = data}).FirstOrDefault();
                //List<modelserviss> data3 = konn.Query<modelserviss>(sql3, new { id = username.Bengkel_id, tp = "2" }).ToList();
                ps.datanota2 = data1;
                ps.datanota1 = data2;
                ps.label5.Text = data.ToString();
                foreach (var item in ps.cbomek.Items)
                {
                    modelmekanik mod = (modelmekanik)item;
                    if(mod.Id_Mekanik == data3.mekanik_id )
                    {
                        ps.cbomek.SelectedItem = item;
                        
                    }

                }
                ps.txtketera.Text = data3.Keterangan.ToString();
             
                ps.resetdatagridview();
                this.Close();

            }
            if (e.ColumnIndex == 8)
            {
                string data1 = advancedDataGridView1[0, e.RowIndex].Value.ToString();
                MessageBox.Show(data1);
              //  string sql1 = "select *from Detail_booking;" ;

               // List<modeldetailboking> data2 = kon.Query<modeldetailboking>(sql1, new { idboking = data1 }).ToList();

                detailbooking dbk = new detailbooking(username,data1);
                dbk.Show();
            }
           if(e.ColumnIndex == 9)
            {
                BengkelEntities5 context = new BengkelEntities5();
                var up = (advancedDataGridView1[0, advancedDataGridView1.CurrentCell.RowIndex].Value.ToString());
                var hapus = (from c in context.Bookings where c.Id_Booking == up select c).FirstOrDefault();
                advancedDataGridView1.Rows.Remove(advancedDataGridView1.Rows[e.RowIndex]);
                hapus.Status_id= 2;
                
                context.SaveChanges();

            }
        }
        
            

        private void advancedDataGridViewSearchToolBar1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void advancedDataGridViewSearchToolBar1_Search(object sender, Zuby.ADGV.AdvancedDataGridViewSearchToolBarSearchEventArgs e)
        {

        }

        private void advancedDataGridView1_FilterStringChanged(object sender, Zuby.ADGV.AdvancedDataGridView.FilterEventArgs e)
        {
            this.bookingBindingSource.Filter = this.advancedDataGridView1.FilterString;
        }

        private void advancedDataGridView1_SortStringChanged(object sender, Zuby.ADGV.AdvancedDataGridView.SortEventArgs e)
        {

            this.bookingBindingSource.Sort = this.advancedDataGridView1.SortString;
        }

        private void fillBy1ToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.vw_notajualTableAdapter.FillBy1(this.bengkelDataSet2.vw_notajual);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        //private void fillBy1ToolStripButton_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.bookingTableAdapter.FillBy1(this.bengkelDataSet.Booking, paramToolStripTextBox.Text, new System.Nullable<int>(((int)(System.Convert.ChangeType(param2ToolStripTextBox.Text, typeof(int))))));
        //    }
        //    catch (System.Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show(ex.Message);
        //    }


    }
    
}
