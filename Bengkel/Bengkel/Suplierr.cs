﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;
using Bengkel.Model;
using Dapper;

namespace Bengkel
{
    public partial class Suplierr : Form
    {
        Karyawan username;
        utama ut;
        public Suplierr(Karyawan username,utama ut)
        {
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            load();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TambahSuplier tam = new TambahSuplier(username,ut,this);
            tam.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int dataa = dataGridView1.CurrentCell.RowIndex;


            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            MessageBox.Show(up.ToString());
            UpSuplier tambah = new UpSuplier(up, username,this);
            tambah.Show();
           
        }
        public void load()
        {
            dataGridView1.DataSource = null;
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select Id_Suplier, Nama_Suplier,Alamat,Telpon from Suplier where Bengkel_id=@id and status=0;";
            List<Modelsuplier> data = kon.Query<Modelsuplier>(sql, new { id = username.Bengkel_id }).ToList();
            kon.Close();
            dataGridView1.DataSource = data;



        }

        private void Suplierr_Load(object sender, EventArgs e)
        {
            ut.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            BengkelEntities5 context = new BengkelEntities5();
            var up = (dataGridView1[0, dataGridView1.CurrentCell.RowIndex].Value.ToString());
            var hapus = (from c in context.Supliers where c.Id_Suplier == up select c).FirstOrDefault();
            hapus.status = 1;
            context.SaveChanges();
            MessageBox.Show("Berhasil Dihapus");
            load();

        }

        private void Suplierr_FormClosing(object sender, FormClosingEventArgs e)
        {
            ut.Show();
        }
    }
}
