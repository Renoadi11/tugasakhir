﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;
using System.Configuration;
using System.Data.SqlClient;
using Dapper;
using Bengkel.Model;
using System.Data.SqlClient;

namespace Bengkel
{
    public partial class formtambahKaryawan : Form
    {
        Karyawann k;
        Karyawan username;
        public formtambahKaryawan(Karyawan username,Karyawann k)
        {
            this.k = k;
            this.username = username;
            InitializeComponent();
            defaultform_load();
        }

        private void TambahKaryawan_Load(object sender, EventArgs e)
        {

        }
        public string get_IDKaryawan()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetidKaryawan id";
          
            string id = kon.Query<string>(sql).FirstOrDefault();
          
            return  "KR"+id;
            
        }
        public void defaultform_load()
        {
            string bdd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bdd);
            kon.Open();
            txtikaryawan.Text = get_IDKaryawan();
            string sql2 = "select *from Master_Bengkel";
            List<Modelbengkel> data3 = kon.Query<Modelbengkel>(sql2, new { id = username.Bengkel_id }).ToList();

            kon.Close();
            foreach (var item in data3)
            {
                cbobengkel.Items.Add(item);
            }

        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            try
            {
                //menginputkan data ke database
                BengkelEntities5 context = new BengkelEntities5();

                Karyawan data = cekvalidasi();
                if (data != null)
                {
                    using (var ctx = new BengkelEntities5())
                    {
                        ctx.Karyawans.Add(data);
                        ctx.SaveChanges();
                        MessageBox.Show("Test");
                    }
                    MessageBox.Show("Data berhasil ditambah ...");
                }



            }
            catch (Exception ex)
            {

                MessageBox.Show("Isi Semua Tabel Terlebih dahulu");
            }

        }
        public Karyawan cekvalidasi()
        {

            string id = txtikaryawan.Text;
            string nama = txtnama.Text;
            string password = txtsandi.Text;
            string konfirm = txtkonfirmsandi.Text;
            string alamat = txtalamat.Text;
            string telpon = txttelpon.Text;



            Karyawan cek = new Karyawan();
            Modelbengkel BENGKEL = (Modelbengkel)cbobengkel.SelectedItem;
            cek.Bengkel_id = BENGKEL.Id_Bengkel;
            cek.Id_Karyawan = id;
            cek.Nama = nama;
            cek.Kata_sandi = password;
            cek.konfirmasi = konfirm;
            cek.Alamat = alamat;
            cek.Telpon = telpon;
            if (radioButton1.Checked || radioButton2.Checked)
            {
                if (radioButton1.Checked)
                {
                    cek.Status_pengguna = 1;

                }
                else if (radioButton2.Checked)
                {
                    cek.Status_pengguna = 0;
                }

            }
            else
            {
                cek = null;
                MessageBox.Show("Pilih status");
            }


            if (nama == "")
            {
                cek = null;
                MessageBox.Show("Nama Isi dulu");
            }
            else if (password == "")
            {
                cek = null;
                MessageBox.Show("Password isi dulu");
            }
            else if (konfirm == "")
            {
                cek = null;
                MessageBox.Show("konfimasi isi dulu");
            }
            else if (alamat == "")
            {
                cek = null;
                MessageBox.Show("Alamat Isi Dulu");
            }
            else if (telpon == "")
            {
                cek = null;
                MessageBox.Show("Isi Dulu Telepon");
            }
            if (cek.Kata_sandi != cek.konfirmasi)
            {
                cek = null;
                MessageBox.Show("Tidak valid");
            }


            clear();
            return cek;


        }

        private void txttelpon_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txttelpon_KeyPress(object sender, KeyPressEventArgs e)
        {
            {
                e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
            }
        }
        public void clear()
        {
            txtnama.Text = "";
            txtsandi.Text = "";
            txtkonfirmsandi.Text = "";
            txtalamat.Text = "";
            txttelpon.Text = "";
            

        }

        private void formtambahKaryawan_FormClosing(object sender, FormClosingEventArgs e)
        {
            k.load();
        }
    }
    
}
