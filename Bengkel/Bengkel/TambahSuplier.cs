﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Dapper;
using System.Configuration;

namespace Bengkel
{
    public partial class TambahSuplier : Form
    {
        Karyawan username;
        utama ut;
        Suplierr s;
        public TambahSuplier(Karyawan username,utama ut, Suplierr s)
        {
            this.s = s;
            this.ut = ut;
            this.username = username;
            InitializeComponent();
            defaultform_load();

        }
        public string get_IdSuplier()
        {
            //menyambungkan ke db
            string bd = ConfigurationManager.ConnectionStrings["ConnStringDb1"].ConnectionString;
            SqlConnection kon = new SqlConnection(bd);
            kon.Open();
            string sql = "select next value for GetIdSuplier id";
            string id = kon.Query<string>(sql).FirstOrDefault();
            return "SP" + id;
        }
        private void defaultform_load()
        {
            txtkode.Text = get_IdSuplier();
        }

        private void btnbatal_Click(object sender, EventArgs e)
        {
            Suplierr kem = new Suplierr(username,ut);
            kem.Show();
            kem.Close();
        }

        private void btnsimpan_Click(object sender, EventArgs e)
        {
            //menambahkan ke db
           
            BengkelEntities5 context = new BengkelEntities5();
            Suplier sup = new Suplier();

           
            //sup.Nama_Suplier = txtnama.Text;
            if(txtnama.Text=="")
            {
                MessageBox.Show("ISI NAMA DULU");
                txtnama.Focus();
            }
           
           // sup.Alamat = txtalamat.Text;
           if(txtalamat.Text=="")
            {
                MessageBox.Show("ISI ALAMAT DULU");
                txtalamat.Focus();
            }
            if (rdoaktif.Checked || rdotidak.Checked)
            {
                if (rdoaktif.Checked)
                {
                    sup.status = 0;


                }
                else if (rdotidak.Checked)
                {
                    sup.status = 1;
                }

            }
            else
            {

               // sup = null;
                MessageBox.Show("Pilih status");
               
            }
            if (txttelpon.Text=="")
            {
                MessageBox.Show("ISI TELEPON DULU");
            }
           else
            {
                sup.Id_Suplier = get_IdSuplier();
                sup.Nama_Suplier = txtnama.Text;
                sup.Telpon = txttelpon.Text;
                sup.Alamat = txtalamat.Text;
                sup.Bengkel_id = username.Bengkel_id;
                context.Supliers.Add(sup);
                context.SaveChanges();
                MessageBox.Show("data Berhasil di input");
                Clear();
            }
           
           
          //  sup.Telpon = txttelpon.Text;
           

           
          


           

        }
        public void Clear()
        {
            txtnama.Text = "";
            txtalamat.Text = "";
            txttelpon.Text = "";
        }

        private void txttelpon_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back;
        }

        private void TambahSuplier_FormClosing(object sender, FormClosingEventArgs e)
        {
            s.load();
        }
    }
    
}
